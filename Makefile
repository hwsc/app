SHELL := /bin/bash
ACCEPTED_COV := $(shell v='$(ACCEPTED_COV)'; echo "90")
PROJ_PATH := $(shell v='$(PROJ_PATH)'; echo ".")
HTML_PATH := $(shell v='$(HTML_PATH)'; echo "public")
BUILD_NAME := $(shell v='$(BUILD_NAME)'; echo "build.out")
BUILD_TAGS := $(shell v='$(BUILD_TAGS)'; echo "integration")

ifndef ACCEPTED_COV
$(error ACCEPTED_COV is not set)
endif

ifndef PROJ_PATH
$(error PROJ_PATH is not set)
endif

ifndef HTML_PATH
$(error HTML_PATH is not set)
endif

ifndef BUILD_NAME
$(error BUILD_NAME is not set)
endif

ifndef BUILD_TAGS
$(error BUILD_TAGS is not set)
endif

.PHONY: all tidy dep build page test lint gosec

all: build

gosec: ## Inspects source code for security problems by scanning the Go AST
	@GO111MODULE=off go get -u github.com/securego/gosec/cmd/gosec
	@GO111MODULE=on gosec -tests ${PROJ_PATH}/...

lint: ## Lint the files
	@result=$$((go mod tidy -v) 2>&1); if [ ! -z "$$result" ]; \
	then \
		>&2 echo "Run make tidy"; \
		>&2 echo "Unused dependencies:"; \
		>&2 echo "$$result"; \
		exit 1; \
	fi
	@golint -set_exit_status $$(go list -tags ${BUILD_TAGS} ${PROJ_PATH}/...)
	@go vet -tags ${BUILD_TAGS} $$(go list -tags ${BUILD_TAGS} ${PROJ_PATH}/...)
	@result=$$(go fmt $$(go list -tags ${BUILD_TAGS} ${PROJ_PATH}/...)); if [ ! -z "$$result" ]; \
	then \
		>&2 echo "Fix formatting error(s):"; \
		>&2 echo "$$result"; \
		exit 1; \
	fi

test: ## Run unit tests
	@go test -v -race -cover -coverprofile ${PROJ_PATH}/cp.out ${PROJ_PATH}/... | go-junit-report -set-exit-code > ${PROJ_PATH}/report.xml
	@go tool cover -func=${PROJ_PATH}/cp.out &> ${PROJ_PATH}/cov.out
	@cat ${PROJ_PATH}/cov.out
	@ACTUAL_COV=$$(cat ${PROJ_PATH}/cov.out | grep  "total.*" |  awk '{print substr($$3, 1, length($$3)-1)}'); \
	echo "$${ACTUAL_COV}" > ${PROJ_PATH}/cov.out; \
	if [ $$(echo "$${ACTUAL_COV}>=${ACCEPTED_COV}" | bc) -eq 0 ]; \
	then \
		>&2 echo "Code coverage less than ${ACCEPTED_COV}%"; \
		exit 1; \
	fi

page: test ## Run unit tests make GitLab page
	@mkdir -p ${PROJ_PATH}/${HTML_PATH}
	@go tool cover -html=${PROJ_PATH}/cp.out -o ${PROJ_PATH}/${HTML_PATH}/index.html

tidy:
	@go mod tidy -v

dep: tidy ## Get the dependencies
	@go get -v -d -t ./...

build: dep ## Build the binary file
	@go build -i -v -o ${PROJ_PATH}/${BUILD_NAME} ${PROJ_PATH}

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
