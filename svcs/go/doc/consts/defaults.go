package consts

import "time"

const (
	// DefaultDocSvcAddr is the default address to use for testing and local development
	DefaultDocSvcAddr = ""
	// DefaultDocSvcPort is the default port to use for testing and local development
	DefaultDocSvcPort = "50051"
	// DefaultDocSvcNetwork is the default network to use for testing and local development
	DefaultDocSvcNetwork = "tcp"
	// DefaultMongoDBHost is the default host for mongodb for testing and local development
	DefaultMongoDBHost = "127.0.0.1"
	// DefaultMongoDBPort is the default port for mongodb for testing and local development
	DefaultMongoDBPort = "27017"
	// DefaultMongoDBWriter is the default mongodb writer to use for testing and local development
	DefaultMongoDBWriter = "testDocumentWriter"
	// DefaultMongoDBWriterPassword is the default password for mongodb writer for testing and local development
	DefaultMongoDBWriterPassword = "testDocumentPwd"
	// DefaultMongoDBReader is the default mongodb reader to use for testing and local development
	DefaultMongoDBReader = "testDocumentReader"
	// DefaultMongoDBReaderPassword is the default password for mongodb reader for testing and local development
	DefaultMongoDBReaderPassword = "testDocumentPwd"
	// DefaultMongoDBName is the default mongodb database to use for testing and local development
	DefaultMongoDBName = "docDB"
	// DefaultMongoDBColl is the default mongodb collection to use for testing and local development
	DefaultMongoDBColl = "docCollection"
	// DefaultDocumentsPerPage is the default number of documents per page
	DefaultDocumentsPerPage uint32 = 15
	// DefaultMaxDocumentsPerPage is the default maximum number of documents per page that can be requested
	DefaultMaxDocumentsPerPage uint32 = 100
	// DefaultMaxTargetPage is the default maximum limit for a target page (1st to 100,000th page)
	DefaultMaxTargetPage uint32 = 100000
	// DefaultMaxAwaitTime is the default time to wait for finding documents from MongoDB
	DefaultMaxAwaitTime = 3 * time.Second
)
