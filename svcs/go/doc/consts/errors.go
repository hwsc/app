package consts

import "errors"

var (
	// ErrNilMongoDatabase for nil MongoDB database wrapper from the client.
	ErrNilMongoDatabase = errors.New("nil Mongo database")
	// ErrNilMongoDBClient for nil MongoDB client.
	ErrNilMongoDBClient = errors.New("nil MongoDB client")
	// ErrInvalidMongoDatabaseStr for invalid Mongo database string.
	ErrInvalidMongoDatabaseStr = errors.New("invalid Mongo database string")
	// ErrInvalidMongoDBCollectionStr for invalid MongoDB collection string.
	ErrInvalidMongoDBCollectionStr = errors.New("invalid MongoDB collection string")
	// ErrInvalidDocumentInstrumentType indicates invalid document instrument type.
	ErrInvalidDocumentInstrumentType = errors.New("invalid document instrument type")
	// ErrInvalidDocumentSamplingRate indicates invalid document sampling rate.
	ErrInvalidDocumentSamplingRate = errors.New("invalid document sampling rate")
	// ErrInvalidDocumentSite indicates invalid document site.
	ErrInvalidDocumentSite = errors.New("invalid document site")
	// ErrInvalidDocumentGroundType indicates invalid document ground type.
	ErrInvalidDocumentGroundType = errors.New("invalid document ground type")
	// ErrInvalidDocumentCountry indicates invalid document country.
	ErrInvalidDocumentCountry = errors.New("invalid document country")
	// ErrInvalidDocumentPopulation indicates invalid distinct population defined by NOAA.
	ErrInvalidDocumentPopulation = errors.New("invalid document distinct population")
	// ErrInvalidDocumentCallTypeName indicates invalid document call type name.
	ErrInvalidDocumentCallTypeName = errors.New("invalid document call type name")
	// ErrInvalidDescription indicates invalid document description.
	ErrInvalidDescription = errors.New("invalid document description")
	// ErrInvalidDocumentRecordTimestamp indicates invalid document record timestamp.
	ErrInvalidDocumentRecordTimestamp = errors.New("invalid document record timestamp")
	// ErrInvalidDocumentCreateTimestamp indicates invalid document create timestamp.
	ErrInvalidDocumentCreateTimestamp = errors.New("invalid document create timestamp")
	// ErrInvalidUpdateTimestamp indicates invalid document update timestamp.
	ErrInvalidUpdateTimestamp = errors.New("invalid document update timestamp")
	// ErrInvalidDocumentLatitude indicates invalid document latitude.
	ErrInvalidDocumentLatitude = errors.New("invalid document latitude")
	// ErrInvalidDocumentLongitude indicates invalid document longitude.
	ErrInvalidDocumentLongitude = errors.New("invalid document longitude")
	// ErrInvalidDocumentSensorName indicates invalid document sensor name.
	ErrInvalidDocumentSensorName = errors.New("invalid document sensor name")
	// ErrInvalidDocumentAudioMap indicates invalid document audio map.
	ErrInvalidDocumentAudioMap = errors.New("invalid document audio map")
	// ErrInvalidDocumentImageMap indicates invalid document image map.
	ErrInvalidDocumentImageMap = errors.New("invalid document image map")
	// ErrInvalidDocumentVideoMap indicates invalid document video map.
	ErrInvalidDocumentVideoMap = errors.New("invalid document video map")
	// ErrInvalidDocumentFileMap indicates invalid document file map.
	ErrInvalidDocumentFileMap = errors.New("invalid document file map")
	// ErrInvalidRequestPage indicates the request asked for an invalid page.
	ErrInvalidRequestPage = errors.New("invalid request page")
	// ErrInvalidRequestDocumentsPerPage indicates the request asked for more than the allowable documents per page.
	ErrInvalidRequestDocumentsPerPage = errors.New("invalid request documents per page")
)
