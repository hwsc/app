package consts

const (
	// MaxInstrumentTypeStrLength is the maximum chars for instrument type.
	MaxInstrumentTypeStrLength = 64
	// MaxSamplingRate is the maximum value for sampling rate in KiloHertz.
	MaxSamplingRate = 4000000000
	// MaxSiteStrLength is the maximum chars for site.
	MaxSiteStrLength = 64
	// MaxGroundTypeStrLength is the maximum chars for ground  type.
	MaxGroundTypeStrLength = 64
	// MaxCountryStrLength is the maximum chars for country.
	MaxCountryStrLength = 64
	// MaxCallTypeNameStrLength is the maximum chars for call type name.
	MaxCallTypeNameStrLength = 64
	// MaxDescriptionStrLength is the maximum chars for description.
	MaxDescriptionStrLength = 15000
	// MinTimestamp in seconds (Jan 1, 1990).
	MinTimestamp = 631152000
	// MaxLatitude is the maximum value for latitude in degrees.
	MaxLatitude = 90
	// MinLatitude is the minimum value for latitude in degrees.
	MinLatitude = -90
	// MaxLongitude is the maximum value for longitude in degrees.
	MaxLongitude = 180
	// MinLongitude is the minimum value for longitude in degrees.
	MinLongitude = -180
	// MaxSensorNameStrLength is the maximum chars for sensor name.
	MaxSensorNameStrLength = 64
)
