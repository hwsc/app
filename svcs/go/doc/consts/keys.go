package consts

const (
	// KeyDocAddr document service environment var key for address
	KeyDocAddr = "HWSC_HOST_DOC_ADDR"
	// KeyDocPort document service environment var key for port
	KeyDocPort = "HWSC_HOST_DOC_PORT"
	// KeyDocNetwork document service environment var key for network
	KeyDocNetwork = "HWSC_HOST_DOC_NETWORK"
	// KeyDocMongoDBHost document service environment var key for mongodb host
	KeyDocMongoDBHost = "HWSC_HOST_DOC_MONGODB_HOST"
	// KeyDocMongoDBPort document service environment var key for mongodb port
	KeyDocMongoDBPort = "HWSC_HOST_DOC_MONGODB_PORT"
	// KeyDocMongoDBWriter document service environment var key for mongodb uri writer
	KeyDocMongoDBWriter = "HWSC_HOST_DOC_MONGODB_WRITER"
	// KeyDocMongoDBWriterPassword document service environment var key for mongodb writer password
	/* #nosec */
	KeyDocMongoDBWriterPassword = "HWSC_HOST_DOC_MONGODB_WRITER_PASSWORD"
	// KeyDocMongoDBReader document service environment var key for mongodb uri reader
	KeyDocMongoDBReader = "HWSC_HOST_DOC_MONGODB_READER"
	// KeyDocMongoDBReaderPassword document service environment var key for mongodb reader password
	/* #nosec */
	KeyDocMongoDBReaderPassword = "HWSC_HOST_DOC_MONGODB_READER_PASSWORD"
	// KeyDocMongoDBName document service environment var key for mongodb database name
	KeyDocMongoDBName = "HWSC_HOST_DOC_MONGODB_NAME"
	// KeyDocMongoDBCollection document service environment var key for mongodb database collection
	KeyDocMongoDBCollection = "HWSC_HOST_DOC_MONGODB_COLLECTION"
)
