// Package service contains the implementations document service defined from the GRPC protocol buffers.
//
// It is highly important that we pass the context object from the request to the last layer of the backend.
//  func GetDocumentOrSomeGRPCProcedure(ctx context.Context, ...) {
//      // you can modify ctx and pass it to the next layer
//      res, err := _docCollectionReader.FindDocument(ctx, bson.D{{}})
//      // add more codes and handle ctx appropriately...
//  }
package service
