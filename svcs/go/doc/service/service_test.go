package service

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	lib "gitlab.com/hwsc/app/lib/go/consts"
	"gitlab.com/hwsc/app/lib/protobufs/svcs/doc"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
	"gitlab.com/hwsc/app/svcs/go/doc/db/mongodb"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

const (
	michaelBanningUUID = "dd8b9d8c-165b-4b0a-afe3-f164de2113a2"
	knownDUID          = "5eb4ad9ffbed2480bb257b09"
)

func TestInitialize(t *testing.T) {
	a := assert.New(t)
	t.Log("should not initialize collection reader and writer due to an error in creating a document collection client")
	{
		originalNewDocumentCollectionClient := _newDocumentCollectionClient
		_newDocumentCollectionClient = func(uri *string) (mongodb.DocumentDatabaseInterface, error) {
			return nil, errors.New("")
		}
		originalLogFatalF := _logFatalF
		_logFatalF = func(v ...interface{}) {
			return
		}
		initialize()
		a.Nil(_docCollectionReader, "MongoDB document collection reader should be nil")
		a.Nil(_docCollectionWriter, "MongoDB document collection writer should be nil")
		_newDocumentCollectionClient = originalNewDocumentCollectionClient
		_logFatalF = originalLogFatalF
	}

	t.Log("should initialize MongoDB document collection reader and writer")
	{
		initialize()
		a.NotNil(_docCollectionReader, "MongoDB document collection reader should not be nil")
		a.NotNil(_docCollectionWriter, "MongoDB document collection writer should not be nil")
	}
}

type mockDocumentDatabase struct {
	mock.Mock
}

func (m *mockDocumentDatabase) FindDocument(ctx context.Context, duid string) (*doc.Document, error) {
	args := m.Called(ctx, duid)
	if args.Get(0) != nil {
		return args.Get(0).(*doc.Document), args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *mockDocumentDatabase) FindUserDocuments(ctx context.Context, userID string, page, documentsPerPage uint32) ([]*doc.Document, error) {
	args := m.Called(ctx, userID, page, documentsPerPage)
	if args.Get(0) != nil {
		return args.Get(0).([]*doc.Document), args.Error(1)
	}
	return nil, args.Error(1)
}

func (m *mockDocumentDatabase) CountUserDocuments(ctx context.Context, userID string) (uint64, error) {
	args := m.Called(ctx, userID)
	return args.Get(0).(uint64), args.Error(1)
}

type unimplementedTestSuite struct {
	suite.Suite
	service          *Service
	errUnImplemented error
}

func TestUnimplementedTestSuite(t *testing.T) {
	suite.Run(t, new(unimplementedTestSuite))
}

func (suite *unimplementedTestSuite) SetupTest() {
	suite.service = &Service{}
	suite.errUnImplemented = status.Error(codes.Unimplemented, "")
}

func (suite *unimplementedTestSuite) TestService_GetStatus() {
	_, err := suite.service.GetStatus(context.TODO(), &doc.DocumentRequest{})
	assert.EqualError(suite.T(), err, suite.errUnImplemented.Error())
}

func (suite *unimplementedTestSuite) TestService_CreateDocument() {
	_, err := suite.service.CreateDocument(context.TODO(), &doc.DocumentRequest{})
	assert.EqualError(suite.T(), err, suite.errUnImplemented.Error())
}

func (suite *unimplementedTestSuite) TestService_UpdateDocument() {
	_, err := suite.service.UpdateDocument(context.TODO(), &doc.DocumentRequest{})
	assert.EqualError(suite.T(), err, suite.errUnImplemented.Error())
}

func (suite *unimplementedTestSuite) TestService_DeleteDocument() {
	_, err := suite.service.DeleteDocument(context.TODO(), &doc.DocumentRequest{})
	assert.EqualError(suite.T(), err, suite.errUnImplemented.Error())
}

func (suite *unimplementedTestSuite) TestService_AddFile() {
	_, err := suite.service.AddFile(context.TODO(), &doc.DocumentRequest{})
	assert.EqualError(suite.T(), err, suite.errUnImplemented.Error())
}

func (suite *unimplementedTestSuite) TestService_DeleteFile() {
	_, err := suite.service.DeleteFile(context.TODO(), &doc.DocumentRequest{})
	assert.EqualError(suite.T(), err, suite.errUnImplemented.Error())
}

type getDocumentTestSuite struct {
	suite.Suite
	service   *Service
	knownDUID string
}

func TestService_GetDocument(t *testing.T) {
	suite.Run(t, new(getDocumentTestSuite))
}

func (suite *getDocumentTestSuite) SetupTest() {
	suite.service = &Service{}
	suite.knownDUID = knownDUID
}

func (suite *getDocumentTestSuite) TestService_GetDocument_Request_Input_Error() {
	t := suite.T()
	a := assert.New(t)
	tests := []struct {
		name     string
		req      *doc.DocumentRequest
		want     *doc.DocumentResponse
		expError error
	}{
		{
			"should fail for invalid duid",
			&doc.DocumentRequest{
				Document: &doc.Document{
					Duid: "5eb4ad9ffbed2480bb257b0z",
				},
			},
			nil,
			status.Error(codes.InvalidArgument, lib.ErrInvalidDUID.Error()),
		},
		{
			"should fail for empty duid",
			&doc.DocumentRequest{
				Document: &doc.Document{
					Duid: "",
				},
			},
			nil,
			status.Error(codes.InvalidArgument, lib.ErrInvalidDUID.Error()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := suite.service.GetDocument(context.TODO(), tt.req)
			a.EqualError(err, tt.expError.Error(), "should have the same error")
			a.Nil(got, "response should be nil")
		})
	}

}

func (suite *getDocumentTestSuite) TestService_GetDocument_FindDocument_Error() {
	t := suite.T()
	a := assert.New(t)
	oldDocCollectionReader := _docCollectionReader
	mockedDocumentDatabase := new(mockDocumentDatabase)
	mockedDocumentDatabase.
		On("FindDocument", mock.Anything, mock.Anything).
		Return(nil, errors.New("")).
		Once()
	_docCollectionReader = mockedDocumentDatabase
	req := &doc.DocumentRequest{
		Document: &doc.Document{
			Duid: suite.knownDUID,
		},
	}
	res, err := suite.service.GetDocument(context.TODO(), req)
	a.Error(err, "should have error from FindDocument")
	a.Nil(res, "response should be nil")
	mockedDocumentDatabase.AssertExpectations(t)
	_docCollectionReader = oldDocCollectionReader
}

func (suite *getDocumentTestSuite) TestService_GetDocument_FindDocument_Success() {
	t := suite.T()
	a := assert.New(t)
	expDocument := &doc.Document{
		Duid:               suite.knownDUID,
		Uuid:               "dd8b9d8c-165b-4b0a-afe3-f164de2113a2",
		InstrumentType:     "some instrument type",
		SamplingRate:       5000,
		Site:               "some site",
		GroundType:         "some ground type",
		Country:            "USA",
		DistinctPopulation: "some distinct population",
		CallTypeName:       "some call type name",
		Description:        "some description",
		RecordTimestamp:    731155000,
		CreateTimestamp:    731155000,
		UpdateTimestamp:    731155000,
		IsPublic:           true,
		Latitude:           60.5,
		Longitude:          90.1,
		SensorName:         "",
		AudioMap:           make(map[string]string),
		ImageMap:           make(map[string]string),
		VideoMap:           make(map[string]string),
		FileMap:            make(map[string]string),
	}
	oldDocCollectionReader := _docCollectionReader
	mockedDocumentDatabase := new(mockDocumentDatabase)
	mockedDocumentDatabase.
		On("FindDocument", mock.Anything, mock.Anything).
		Return(expDocument, nil).
		Once()
	_docCollectionReader = mockedDocumentDatabase
	req := &doc.DocumentRequest{
		Document: &doc.Document{
			Duid: suite.knownDUID,
		},
	}
	res, err := suite.service.GetDocument(context.TODO(), req)
	a.Nil(err, "should have no error from FindDocument")
	a.Equal(expDocument, res.Document, "should find the same values with actual fetched document")
	mockedDocumentDatabase.AssertExpectations(t)
	_docCollectionReader = oldDocCollectionReader
}

type getUserDocumentsTestSuite struct {
	suite.Suite
	service *Service
	userID  string
}

func TestService_GetUserDocuments(t *testing.T) {
	suite.Run(t, new(getUserDocumentsTestSuite))
}

func (suite *getUserDocumentsTestSuite) SetupTest() {
	suite.service = &Service{}
	suite.userID = michaelBanningUUID
}

func (suite *getUserDocumentsTestSuite) TestService_GetUserDocuments_Request_Input_Error() {
	t := suite.T()
	a := assert.New(t)
	type args struct {
		ctx context.Context
		req *doc.DocumentRequest
	}
	tests := []struct {
		name     string
		args     args
		want     *doc.DocumentResponse
		expError error
	}{
		{
			"should fail for invalid UUID",
			args{
				context.TODO(),
				&doc.DocumentRequest{
					Document: &doc.Document{
						Uuid: "1234567890",
					},
				},
			},
			nil,
			status.Error(codes.InvalidArgument, lib.ErrInvalidUUID.Error()),
		},
		{
			"should fail for invalid target page",
			args{
				context.TODO(),
				&doc.DocumentRequest{
					Document: &doc.Document{
						Uuid: suite.userID,
					},
					Page: 0,
				},
			},
			nil,
			status.Error(codes.InvalidArgument, consts.ErrInvalidRequestPage.Error()),
		},
		{
			"should fail due to target page is more than the max limit",
			args{
				context.TODO(),
				&doc.DocumentRequest{
					Document: &doc.Document{
						Uuid: suite.userID,
					},
					Page: consts.DefaultMaxTargetPage + 1,
				},
			},
			nil,
			status.Error(codes.InvalidArgument, consts.ErrInvalidRequestPage.Error()),
		},
		{
			"should fail due to requested documents per page is more than the max limit",
			args{
				context.TODO(),
				&doc.DocumentRequest{
					Document: &doc.Document{
						Uuid: suite.userID,
					},
					Page:             1,
					DocumentsPerPage: consts.DefaultMaxDocumentsPerPage + 1,
				},
			},
			nil,
			status.Error(codes.InvalidArgument, consts.ErrInvalidRequestDocumentsPerPage.Error()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := suite.service.GetUserDocuments(tt.args.ctx, tt.args.req)
			a.EqualError(err, tt.expError.Error(), "should have the same error")
			a.Nil(got, "response should be nil")
		})
	}
}

func (suite *getUserDocumentsTestSuite) TestService_GetUserDocuments_FindUserDocuments_Error() {
	t := suite.T()
	a := assert.New(t)
	oldDocCollectionReader := _docCollectionReader
	mockedDocumentDatabase := new(mockDocumentDatabase)
	mockedDocumentDatabase.
		On("FindUserDocuments", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(nil, errors.New("")).
		Once()
	_docCollectionReader = mockedDocumentDatabase
	req := &doc.DocumentRequest{
		Document: &doc.Document{
			Uuid: suite.userID,
		},
		Page: 1,
	}
	res, err := suite.service.GetUserDocuments(context.TODO(), req)
	a.Error(err, "should have error from FindUserDocuments")
	a.Nil(res, "response should be nil")
	mockedDocumentDatabase.AssertExpectations(t)
	_docCollectionReader = oldDocCollectionReader
}

func (suite *getUserDocumentsTestSuite) TestService_GetUserDocuments_FindUserDocuments_Default_Per_Page_Success() {
	t := suite.T()
	a := assert.New(t)
	mockedDocumentCollection := make([]*doc.Document, consts.DefaultDocumentsPerPage)
	for i := range mockedDocumentCollection {
		mockedDocumentCollection[i] = &doc.Document{}
	}
	oldDocCollectionReader := _docCollectionReader
	mockedDocumentDatabase := new(mockDocumentDatabase)
	mockedDocumentDatabase.
		On("FindUserDocuments", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(mockedDocumentCollection, nil).
		Once()
	_docCollectionReader = mockedDocumentDatabase
	req := &doc.DocumentRequest{
		Document: &doc.Document{
			Uuid: suite.userID,
		},
		Page: 1,
	}
	res, err := suite.service.GetUserDocuments(context.TODO(), req)
	a.Nil(err, "should have no error from FindUserDocuments")
	a.EqualValues(len(mockedDocumentCollection), len(res.DocumentCollection), "should have the same number of documents")
	mockedDocumentDatabase.AssertExpectations(t)
	_docCollectionReader = oldDocCollectionReader
}

func (suite *getUserDocumentsTestSuite) TestService_GetUserDocuments_FindUserDocuments_Set_Documents_Per_Page_Success() {
	t := suite.T()
	a := assert.New(t)
	mockedDocumentCollection := make([]*doc.Document, consts.DefaultMaxDocumentsPerPage)
	for i := range mockedDocumentCollection {
		mockedDocumentCollection[i] = &doc.Document{}
	}
	oldDocCollectionReader := _docCollectionReader
	mockedDocumentDatabase := new(mockDocumentDatabase)
	mockedDocumentDatabase.
		On("FindUserDocuments", mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(mockedDocumentCollection, nil).
		Once()
	_docCollectionReader = mockedDocumentDatabase
	req := &doc.DocumentRequest{
		Document: &doc.Document{
			Uuid: suite.userID,
		},
		Page:             1,
		DocumentsPerPage: consts.DefaultMaxDocumentsPerPage,
	}
	res, err := suite.service.GetUserDocuments(context.TODO(), req)
	a.Nil(err, "should have no error from FindUserDocuments")
	a.EqualValues(len(mockedDocumentCollection), len(res.DocumentCollection), "should have the same number of documents")
	mockedDocumentDatabase.AssertExpectations(t)
	_docCollectionReader = oldDocCollectionReader
}

type countUserDocuments struct {
	suite.Suite
	service *Service
	userID  string
}

func TestService_CountUserDocuments(t *testing.T) {
	suite.Run(t, new(countUserDocuments))
}

func (suite *countUserDocuments) SetupTest() {
	suite.service = &Service{}
	suite.userID = michaelBanningUUID
}

func (suite *countUserDocuments) TestService_CountUserDocuments_Request_Input_Error() {
	t := suite.T()
	a := assert.New(t)
	type args struct {
		ctx context.Context
		req *doc.DocumentRequest
	}
	tests := []struct {
		name     string
		args     args
		want     *doc.DocumentResponse
		expError error
	}{
		{
			"should fail for invalid UUID",
			args{
				context.TODO(),
				&doc.DocumentRequest{
					Document: &doc.Document{
						Uuid: "1234567890",
					},
				},
			},
			nil,
			status.Error(codes.InvalidArgument, lib.ErrInvalidUUID.Error()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := suite.service.CountUserDocuments(tt.args.ctx, tt.args.req)
			a.EqualError(err, tt.expError.Error(), "should have the same error")
			a.Nil(got, "response should be nil")
		})
	}
}

func (suite *countUserDocuments) TestService_CountUserDocuments_CountUserDocuments_Error() {
	t := suite.T()
	a := assert.New(t)
	mockedDocumentDatabase := new(mockDocumentDatabase)
	mockedDocumentDatabase.
		On("CountUserDocuments", mock.Anything, mock.Anything).
		Return(uint64(0), errors.New("")).
		Once()
	oldDocCollectionReader := _docCollectionReader
	_docCollectionReader = mockedDocumentDatabase
	req := &doc.DocumentRequest{
		Document: &doc.Document{
			Uuid: suite.userID,
		},
	}
	res, err := suite.service.CountUserDocuments(context.TODO(), req)
	if a.Error(err, "should get an error due to a database collection query issue") {
		a.Nil(res, "should return nil response due to a database collection query issue")
	}
	mockedDocumentDatabase.AssertExpectations(t)
	_docCollectionReader = oldDocCollectionReader
}

func (suite *countUserDocuments) TestService_CountUserDocuments_Success() {
	t := suite.T()
	a := assert.New(t)
	var expTotalDocuments uint64 = 1500
	mockedDocumentDatabase := new(mockDocumentDatabase)
	mockedDocumentDatabase.
		On("CountUserDocuments", mock.Anything, mock.Anything).
		Return(expTotalDocuments, nil).
		Once()
	oldDocCollectionReader := _docCollectionReader
	_docCollectionReader = mockedDocumentDatabase
	req := &doc.DocumentRequest{
		Document: &doc.Document{
			Uuid: suite.userID,
		},
	}
	res, err := suite.service.CountUserDocuments(context.TODO(), req)
	if a.NoError(err, "should get no error response") {
		a.EqualValues(expTotalDocuments, res.TotalDocuments, "should return expected total number of documents")
	}
	mockedDocumentDatabase.AssertExpectations(t)
	_docCollectionReader = oldDocCollectionReader
}
