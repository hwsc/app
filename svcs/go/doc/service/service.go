package service

import (
	"context"
	"gitlab.com/hwsc/app/lib/protobufs/svcs/doc"
	"gitlab.com/hwsc/app/svcs/go/doc/conf"
	"gitlab.com/hwsc/app/svcs/go/doc/db/mongodb"
	"gitlab.com/hwsc/app/svcs/go/doc/util"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
)

var (
	// _docCollectionReader is a read-only MongoDB document collection reader
	_docCollectionReader mongodb.DocumentDatabaseInterface
	// _docCollectionWriter has write access to MongoDB document collection
	_docCollectionWriter mongodb.DocumentDatabaseInterface

	// _newDocumentCollectionClient makes a client for MongoDB document collection
	_newDocumentCollectionClient = mongodb.NewDocumentCollectionClient
	_logFatalF                   = log.Fatal
)

func init() {
	initialize()
}

func initialize() {
	var err error
	_docCollectionReader, err = _newDocumentCollectionClient(&conf.MongoDBReaderConnStr)
	if err != nil {
		_logFatalF(err)
	}
	_docCollectionWriter, err = _newDocumentCollectionClient(&conf.MongoDBWriterConnStr)
	if err != nil {
		_logFatalF(err)
	}
}

// Service implements services for managing document.
type Service struct{}

// GetStatus for pinging, debugging, and waiting purposes.
// This can be used for integration testing to check for readiness.
func (s *Service) GetStatus(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}

// CreateDocument with the given request payload.
// User name will not be returned due to user information is not stored in MongoDB.
func (s *Service) CreateDocument(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}

// GetDocument using a given document identifier.
func (s *Service) GetDocument(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	duid := req.Document.GetDuid()
	if err := util.ValidateDUID(duid); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	document, err := _docCollectionReader.FindDocument(ctx, duid)
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	return &doc.DocumentResponse{Document: document}, nil
}

// GetUserDocuments using a given user identifier.
func (s *Service) GetUserDocuments(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	userID := req.Document.GetUuid()
	if err := util.ValidateUUID(userID); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	page := req.GetPage()
	if err := util.ValidateRequestPage(page); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	documentsPerPage := req.GetDocumentsPerPage()
	if err := util.ValidateRequestDocumentsPerPage(documentsPerPage); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	userDocuments, err := _docCollectionReader.FindUserDocuments(ctx, userID, page, documentsPerPage)
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	return &doc.DocumentResponse{DocumentCollection: userDocuments}, nil
}

// CountUserDocuments using a given user identifier.
func (s *Service) CountUserDocuments(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	userID := req.Document.GetUuid()
	if err := util.ValidateUUID(userID); err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}
	totalDocuments, err := _docCollectionReader.CountUserDocuments(ctx, userID)
	if err != nil {
		return nil, status.Error(codes.Unavailable, err.Error())
	}
	return &doc.DocumentResponse{TotalDocuments: totalDocuments}, nil
}

// UpdateDocument with request payload document. This is full update and not a patch update.
func (s *Service) UpdateDocument(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}

// DeleteDocument with using the document identifier.
func (s *Service) DeleteDocument(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}

// AddFile to a document.
func (s *Service) AddFile(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}

// DeleteFile from a document.
func (s *Service) DeleteFile(ctx context.Context, req *doc.DocumentRequest) (*doc.DocumentResponse, error) {
	return nil, status.Error(codes.Unimplemented, "")
}
