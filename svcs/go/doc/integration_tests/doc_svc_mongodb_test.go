// +build integration

package integration_tests

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/hwsc/app/lib/protobufs/svcs/doc"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
	"gitlab.com/hwsc/app/svcs/go/doc/service"
	"sort"
	"testing"
)

const (
	michaelBanningUUID = "dd8b9d8c-165b-4b0a-afe3-f164de2113a2"
	unknownUUID        = "12345678-90ab-def1-2345-67890abcdef1"
	existingDUID       = "5eba32d80e2b3a3fdc13b695"
	nonexistentDUID    = "3eba32d80e2b3a3fdc13b695"
)

type getDocumentIntegrationTestSuite struct {
	suite.Suite
	service            *service.Service
	knownDUID          string
	reqWithKnownDUID   *doc.DocumentRequest
	reqWithUnknownDUID *doc.DocumentRequest
	ctx                context.Context
}

func TestIntegrationService_GetDocument(t *testing.T) {
	suite.Run(t, new(getDocumentIntegrationTestSuite))
}

func (suite *getDocumentIntegrationTestSuite) SetupTest() {
	suite.service = &service.Service{}
	suite.knownDUID = existingDUID
	suite.reqWithKnownDUID = &doc.DocumentRequest{Document: &doc.Document{Duid: existingDUID}}
	suite.reqWithUnknownDUID = &doc.DocumentRequest{Document: &doc.Document{Duid: nonexistentDUID}}
	suite.ctx = context.TODO()
}

func (suite *getDocumentIntegrationTestSuite) TestIntegrationService_GetDocument_Known_DUID() {
	t := suite.T()
	a := assert.New(t)
	res, err := suite.service.GetDocument(suite.ctx, suite.reqWithKnownDUID)
	if a.Nil(err, "should not get an error") {
		if a.NotNil(res, "should not get a nil response") {
			a.EqualValues(suite.knownDUID, res.Document.GetDuid(), "should be the same DUID")
		}
	}
}

func (suite *getDocumentIntegrationTestSuite) TestIntegrationService_GetDocument_Unknown_DUID() {
	t := suite.T()
	a := assert.New(t)
	res, err := suite.service.GetDocument(suite.ctx, suite.reqWithUnknownDUID)
	if a.NotNil(err, "should get an error for sending a non-existent DUID") {
		a.Nil(res, "should get a nil response")
	}
}

type getUserDocumentsIntegrationTestSuite struct {
	suite.Suite
	service           *service.Service
	knownID           string
	knownPagesMap     map[uint32]int
	knownNumberOfDocs uint32
	reqWithUnknownID  *doc.DocumentRequest
	ctx               context.Context
}

func TestIntegrationService_GetUserDocuments(t *testing.T) {
	suite.Run(t, new(getUserDocumentsIntegrationTestSuite))
}

func (suite *getUserDocumentsIntegrationTestSuite) SetupTest() {
	suite.service = &service.Service{}
	suite.knownID = michaelBanningUUID
	suite.knownPagesMap = map[uint32]int{1: 15, 2: 7, 3: 0}
	suite.knownNumberOfDocs = 22
	suite.reqWithUnknownID = &doc.DocumentRequest{Document: &doc.Document{Uuid: unknownUUID}, Page: 1}
	suite.ctx = context.TODO()
}

func (suite *getUserDocumentsIntegrationTestSuite) TestIntegrationService_GetUserDocuments_Unknown_User() {
	t := suite.T()
	a := assert.New(t)
	res, err := suite.service.GetUserDocuments(suite.ctx, suite.reqWithUnknownID)
	if a.Nil(err, "should not get an error for sending an unknown UUID") {
		if a.NotNil(res, "should not get a nil response") {
			a.EqualValues(0, len(res.DocumentCollection), "should get no documents")
		}
	}
}

func (suite *getUserDocumentsIntegrationTestSuite) TestIntegrationService_GetUserDocuments_Known_User() {
	t := suite.T()
	a := assert.New(t)
	// Michael Banning has 22 documents
	// First page should return the default number of pages (15)
	// Second page should return the remainder pages (7)
	// Third page should return zero page
	// Documents should be sorted from latest to oldest
	var i uint32 = 1
	for ; i <= uint32(len(suite.knownPagesMap)); i++ {
		req := &doc.DocumentRequest{Document: &doc.Document{Uuid: suite.knownID}, Page: i}
		res, err := suite.service.GetUserDocuments(suite.ctx, req)
		if a.Nil(err, "should not get an error") {
			if a.NotNil(res, "should not get a nil response") {
				documentCollection := res.DocumentCollection
				a.EqualValues(suite.knownPagesMap[i], len(documentCollection), "should get the same expected number of documents")
				sortedByCreateTimestamp := sort.SliceIsSorted(documentCollection, func(i, j int) bool {
					return documentCollection[j].GetCreateTimestamp() < documentCollection[i].GetCreateTimestamp()
				})
				a.True(sortedByCreateTimestamp, "should be sorted from latest to oldest")
			}
		}
	}
}

func (suite *getUserDocumentsIntegrationTestSuite) TestIntegrationService_GetUserDocuments_Known_User_With_Documents_Per_Page() {
	t := suite.T()
	a := assert.New(t)
	// Michael Banning has 22 documents
	// Requesting 100 documents per page should return all the remaining documents
	// First page should return 22 documents
	// Documents should be sorted from latest to oldest
	req := &doc.DocumentRequest{
		Document: &doc.Document{
			Uuid: suite.knownID,
		},
		Page:             1,
		DocumentsPerPage: consts.DefaultMaxDocumentsPerPage,
	}
	res, err := suite.service.GetUserDocuments(suite.ctx, req)
	if a.Nil(err, "should not get an error") {
		if a.NotNil(res, "should not get a nil response") {
			documentCollection := res.DocumentCollection
			a.EqualValues(suite.knownNumberOfDocs, len(documentCollection), "should get the same expected number of documents")
			sortedByCreateTimestamp := sort.SliceIsSorted(documentCollection, func(i, j int) bool {
				return documentCollection[j].GetCreateTimestamp() < documentCollection[i].GetCreateTimestamp()
			})
			a.True(sortedByCreateTimestamp, "should be sorted from latest to oldest")
		}
	}
}

type countUserDocumentsIntegrationTestSuite struct {
	suite.Suite
	service             *service.Service
	knownTotalDocuments uint64
	reqWithKnownID      *doc.DocumentRequest
	reqWithUnknownID    *doc.DocumentRequest
	reqWithInvalidID    *doc.DocumentRequest
	ctx                 context.Context
}

func TestIntegrationService_CountUserDocuments(t *testing.T) {
	suite.Run(t, new(countUserDocumentsIntegrationTestSuite))
}

func (suite *countUserDocumentsIntegrationTestSuite) SetupTest() {
	suite.service = &service.Service{}
	suite.knownTotalDocuments = 22
	suite.reqWithKnownID = &doc.DocumentRequest{Document: &doc.Document{Uuid: michaelBanningUUID}}
	suite.reqWithUnknownID = &doc.DocumentRequest{Document: &doc.Document{Uuid: unknownUUID}}
	suite.ctx = context.TODO()
}

func (suite *countUserDocumentsIntegrationTestSuite) TestIntegrationService_CountUserDocuments_Unknown_User() {
	t := suite.T()
	a := assert.New(t)
	res, err := suite.service.CountUserDocuments(suite.ctx, suite.reqWithUnknownID)
	if a.Nil(err, "should not get an error for sending an unknown UUID") {
		if a.NotNil(res, "should not get a nil response") {
			a.EqualValues(0, res.TotalDocuments, "should get zero documents")
		}
	}
}

func (suite *countUserDocumentsIntegrationTestSuite) TestIntegrationService_CountUserDocuments_Known_User() {
	t := suite.T()
	a := assert.New(t)
	res, err := suite.service.CountUserDocuments(suite.ctx, suite.reqWithKnownID)
	if a.Nil(err, "should not get an error") {
		if a.NotNil(res, "should not get a nil response") {
			a.EqualValues(suite.knownTotalDocuments, res.TotalDocuments, "should get the expected number of documents")
		}
	}
}
