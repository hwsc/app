package util

import (
	lib "gitlab.com/hwsc/app/lib/go/consts"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/url"
	"regexp"
	"strings"
	"time"
)

var (
	uuidRegex             = regexp.MustCompile("^[[:digit:]a-f]{8}-[[:digit:]a-f]{4}-[[:digit:]a-f]{4}-[[:digit:]a-f]{4}-[[:digit:]a-f]{12}$")
	imageRegex            = regexp.MustCompile("^.*\\.(jpg|jpeg|png|bmp|tif|gif|tiff)$")
	audioRegex            = regexp.MustCompile("^.*\\.(wav|wma|ogg|m4a|mp3)$")
	videoRegex            = regexp.MustCompile("^.*\\.(flv|wmv|mov|avi|mp4)$")
	distinctPopulationMap = map[string]bool{
		"west indies":                 true,
		"cape verde/northwest africa": true,
		"western north pacific":       true,
		"hawaii":                      true,
		"mexico":                      true,
		"central america":             true,
		"brazil":                      true,
		"gabon/southwest africa":      true,
		"southeast africa/madagascar": true,
		"west australia":              true,
		"east australia":              true,
		"oceania":                     true,
		"southeastern pacific":        true,
		"arabian sea":                 true,
	}
)

// ValidateDUID validates duid.
// Returns an error if duid fails validation.
func ValidateDUID(duid string) error {
	_, err := primitive.ObjectIDFromHex(duid)
	if err != nil {
		return lib.ErrInvalidDUID
	}
	return nil
}

// ValidateUUID validates uuid.
// Returns an error if uuid fails validation.
func ValidateUUID(uuid string) error {
	if !uuidRegex.MatchString(uuid) {
		return lib.ErrInvalidUUID
	}
	return nil
}

// ValidateInstrumentType validates instrument type.
// Returns an error if instrument type is an empty string or exceeds 64 chars.
func ValidateInstrumentType(instrumentType string) error {
	if strings.TrimSpace(instrumentType) == "" || len(instrumentType) > consts.MaxInstrumentTypeStrLength {
		return consts.ErrInvalidDocumentInstrumentType
	}
	return nil
}

// ValidateSamplingRate validates sampling rate in KHz.
// Returns an error if sampling rate exceeds max sampling rate of 4000000000 KHz.
func ValidateSamplingRate(samplingRate uint32) error {
	if samplingRate > consts.MaxSamplingRate {
		return consts.ErrInvalidDocumentSamplingRate
	}
	return nil
}

// ValidateSite validates study site.
// Returns an error if site is an empty string or exceeds 64 chars.
func ValidateSite(site string) error {
	if strings.TrimSpace(site) == "" || len(site) > consts.MaxSiteStrLength {
		return consts.ErrInvalidDocumentSite
	}
	return nil
}

// ValidateGroundType validates ground type.
// Returns an error if ground type is an empty string or exceeds 64 chars.
func ValidateGroundType(groundType string) error {
	if strings.TrimSpace(groundType) == "" || len(groundType) > consts.MaxGroundTypeStrLength {
		return consts.ErrInvalidDocumentGroundType
	}
	return nil
}

// ValidateCountry validates country or region of study site.
// Returns an error if country is an empty string or exceeds 64 chars.
func ValidateCountry(country string) error {
	if strings.TrimSpace(country) == "" || len(country) > consts.MaxCountryStrLength {
		return consts.ErrInvalidDocumentCountry
	}
	return nil
}

// ValidateDistinctPopulation validates humpback whale population defined by NOAA.
// https://www.fisheries.noaa.gov/resource/map/humpback-whale-distinct-population-segments-identification-map
// Returns an error if input is not listed as one of the 14 distinct populations.
func ValidateDistinctPopulation(distinctPopulation string) error {
	if strings.TrimSpace(distinctPopulation) == "" {
		return consts.ErrInvalidDocumentPopulation
	}
	normalizedStr := strings.ToLower(strings.Join(strings.Fields(distinctPopulation), " "))
	if !distinctPopulationMap[normalizedStr] {
		return consts.ErrInvalidDocumentPopulation
	}
	return nil
}

// ValidateCallTypeName validates call type name.
// Returns an error if call type name is an empty string or exceeds 64 chars.
func ValidateCallTypeName(callTypeName string) error {
	if strings.TrimSpace(callTypeName) == "" || len(callTypeName) > consts.MaxCallTypeNameStrLength {
		return consts.ErrInvalidDocumentCallTypeName
	}
	return nil
}

// ValidateDescription validates length of description.
// Returns an error if description exceeds 15000 chars.
func ValidateDescription(desc string) error {
	if len(desc) > consts.MaxDescriptionStrLength {
		return consts.ErrInvalidDescription
	}
	return nil
}

// ValidateRecordTimestamp validates record timestamp.
// Returns an error if timestamp is set before Jan 1, 1990 or now.
func ValidateRecordTimestamp(timestamp int64) error {
	if timestamp < consts.MinTimestamp || timestamp > time.Now().Unix() {
		return consts.ErrInvalidDocumentRecordTimestamp
	}

	return nil
}

// ValidateCreateTimestamp validates create timestamp.
// Returns an error if create timestamp is set before record timestamp, or create timestamp is set after now.
func ValidateCreateTimestamp(createTimestamp int64, recordTimeStamp int64) error {
	if createTimestamp == 0 {
		return nil
	}
	if createTimestamp < recordTimeStamp || createTimestamp > time.Now().Unix() {
		return consts.ErrInvalidDocumentCreateTimestamp
	}
	return nil
}

// ValidateUpdateTimestamp validates create timestamp.
// Returns an error if create timestamp is set after update timestamp, or update timestamp is set after now.
func ValidateUpdateTimestamp(updateTimestamp int64, createTimestamp int64) error {
	if updateTimestamp == 0 {
		return nil
	}
	if createTimestamp > updateTimestamp || updateTimestamp > time.Now().Unix() {
		return consts.ErrInvalidUpdateTimestamp
	}
	return nil
}

// ValidateLatitude validates latitude.
// Returns an error if latitude is not within [-90,90].
func ValidateLatitude(latitude float32) error {
	if latitude > consts.MaxLatitude || latitude < consts.MinLatitude {
		return consts.ErrInvalidDocumentLatitude
	}
	return nil
}

// ValidateLongitude validates longitude.
// Returns an error if a longitude is not within [-180,180].
func ValidateLongitude(longitude float32) error {
	if longitude > consts.MaxLongitude || longitude < consts.MinLongitude {
		return consts.ErrInvalidDocumentLongitude
	}
	return nil
}

// ValidateSensorName validates the optional field sensor name.
// Returns an error if sensor name exceeds 64 chars.
func ValidateSensorName(sensorName string) error {
	if len(sensorName) > consts.MaxSensorNameStrLength {
		return consts.ErrInvalidDocumentSensorName
	}
	return nil
}

func isValidMediaMap(mediaMap map[string]string, mediaRegex *regexp.Regexp) bool {
	if mediaMap == nil {
		return false
	}
	for k, v := range mediaMap {
		if !uuidRegex.MatchString(k) {
			return false
		}
		if strings.TrimSpace(v) == "" {
			return false
		}
		if mediaRegex != nil && !mediaRegex.MatchString(strings.ToLower(v)) {
			return false
		}
		if _, err := url.ParseRequestURI(v); err != nil {
			return false
		}
	}
	return true
}

// ValidateAudioMap validates document's key-value pairs of UUID and audio URL.
// Returns an error if a URL or UUID is an empty string, or unsupported format.
func ValidateAudioMap(audioMap map[string]string) error {
	if !isValidMediaMap(audioMap, audioRegex) {
		return consts.ErrInvalidDocumentAudioMap
	}
	return nil
}

// ValidateImageMap validates document's key-value pairs of UUID and image URL.
// Returns an error if a URL or UUID is an empty string, unsupported format.
func ValidateImageMap(imageMap map[string]string) error {
	if !isValidMediaMap(imageMap, imageRegex) {
		return consts.ErrInvalidDocumentImageMap
	}
	return nil
}

// ValidateVideoMap validates document's key-value pairs of UUID and video URL.
// Returns an error if a URL or UUID is an empty string, unsupported format.
func ValidateVideoMap(videoMap map[string]string) error {
	if !isValidMediaMap(videoMap, videoRegex) {
		return consts.ErrInvalidDocumentVideoMap
	}
	return nil
}

// ValidateFileMap validates document's key-value pairs of UUID and file URL.
// Returns an error if a URL or UUID is an empty string, unsupported format.
func ValidateFileMap(videoMap map[string]string) error {
	if !isValidMediaMap(videoMap, nil) {
		return consts.ErrInvalidDocumentFileMap
	}
	return nil
}

// ValidateRequestPage validates target page prior to querying MongoDB.
func ValidateRequestPage(page uint32) error {
	if page == 0 || page > consts.DefaultMaxTargetPage {
		return consts.ErrInvalidRequestPage
	}
	return nil
}

// ValidateRequestDocumentsPerPage ensures request does not go more than the allowable limit.
func ValidateRequestDocumentsPerPage(documentsPerPage uint32) error {
	if documentsPerPage > consts.DefaultMaxDocumentsPerPage {
		return consts.ErrInvalidRequestDocumentsPerPage
	}
	return nil
}
