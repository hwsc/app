package util

import (
	"github.com/stretchr/testify/assert"
	lib "gitlab.com/hwsc/app/lib/go/consts"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"testing"
	"time"
)

var (
	// read from text files instead of declaring an extremely long string for document description
	validDocumentDescription   []byte
	invalidDocumentDescription []byte
)

func TestMain(t *testing.M) {
	var err error
	validDocumentDescription, err = ioutil.ReadFile("./test_fixtures/valid_description.txt")
	if err != nil {
		log.Fatal(err)
	}
	invalidDocumentDescription, err = ioutil.ReadFile("./test_fixtures/invalid_description.txt")
	if err != nil {
		log.Fatal(err)
	}
	code := t.Run()
	os.Exit(code)
}

func TestValidateDUID(t *testing.T) {
	tests := []struct {
		name    string
		duid    string
		wantErr bool
	}{
		{
			"should fail for empty string",
			"",
			true,
		},
		{
			"should fail for string with only spaces",
			"      ",
			true,
		},
		{
			"should fail for invalid format",
			"2af65e30-b645-4441-90c9-31181e2e670d",
			true,
		},
		{
			"should pass validation",
			"5e892e64b27bf1cdccc04535",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := ValidateDUID(tt.duid); (err != nil) != tt.wantErr {
				t.Errorf("ValidateDUID() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestValidateUUID(t *testing.T) {
	tests := []struct {
		name    string
		uuid    string
		wantErr bool
		expErr  error
	}{
		{
			"should fail due to empty string",
			"",
			true,
			lib.ErrInvalidUUID,
		},
		{
			"should fail due for string with only spaces",
			"   ",
			true,
			lib.ErrInvalidUUID,
		},
		{
			"should fail due to non-hex character",
			"2zf65e30-b645-4441-90c9-31181e2e670d",
			true,
			lib.ErrInvalidUUID,
		},
		{
			"should fail due to extra char",
			"2zf65e30-b645-4441-90c9-31181e2e670ds",
			true,
			lib.ErrInvalidUUID,
		},
		{
			"should fail due to unsupported uuid format",
			"2af65e30b645444190c931181e2e670d",
			true,
			lib.ErrInvalidUUID,
		},
		{
			"should pass validation",
			"2af65e30-b645-4441-90c9-31181e2e670d",
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateUUID(tt.uuid)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateUUID() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateInstrumentType(t *testing.T) {
	tests := []struct {
		name           string
		instrumentType string
		wantErr        bool
		expErr         error
	}{
		{
			"should fail due to an empty string",
			"",
			true,
			consts.ErrInvalidDocumentInstrumentType,
		},
		{
			"should fail due to string with only spaces",
			"    ",
			true,
			consts.ErrInvalidDocumentInstrumentType,
		},
		{
			"should fail due to exceeded maximum length of string",
			"12345678912345678901234567890123412345678912345678901234567890123",
			true,
			consts.ErrInvalidDocumentInstrumentType,
		},
		{
			"should pass validation",
			"Bprobe",
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateInstrumentType(tt.instrumentType)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateInstrumentType() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateSamplingRate(t *testing.T) {
	tests := []struct {
		name         string
		samplingRate uint32
		wantErr      bool
		expErr       error
	}{
		{
			"should pass for 0",
			0,
			false,
			nil,
		},
		{
			"should pass for valid value",
			10000,
			false,
			nil,
		},
		{
			"should pass for max value",
			consts.MaxSamplingRate,
			false,
			nil,
		},
		{
			"should fail due more than max value",
			consts.MaxSamplingRate + 1,
			true,
			consts.ErrInvalidDocumentSamplingRate,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateSamplingRate(tt.samplingRate)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateSamplingRate() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateSite(t *testing.T) {
	tests := []struct {
		name    string
		site    string
		wantErr bool
		expErr  error
	}{
		{
			"should fail due to an empty string",
			"",
			true,
			consts.ErrInvalidDocumentSite,
		},
		{
			"should fail due to string with only spaces",
			"    ",
			true,
			consts.ErrInvalidDocumentSite,
		},
		{
			"should fail due to exceeded maximum length of string",
			"12345678912345678901234567890123412345678912345678901234567890123",
			true,
			consts.ErrInvalidDocumentSite,
		},
		{
			"should pass validation",
			"Some site",
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateSite(tt.site)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateSite() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateGroundType(t *testing.T) {
	tests := []struct {
		name       string
		groundType string
		wantErr    bool
		expErr     error
	}{
		{
			"should fail due to an empty string",
			"",
			true,
			consts.ErrInvalidDocumentGroundType,
		},
		{
			"should fail due to string with only spaces",
			"    ",
			true,
			consts.ErrInvalidDocumentGroundType,
		},
		{
			"should fail due to exceeded maximum length of string",
			"12345678912345678901234567890123412345678912345678901234567890123",
			true,
			consts.ErrInvalidDocumentGroundType,
		},
		{
			"should pass validation",
			"Some ground type",
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateGroundType(tt.groundType)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateGroundType() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateCountry(t *testing.T) {
	tests := []struct {
		name    string
		country string
		wantErr bool
		expErr  error
	}{
		{
			"should fail due to an empty string",
			"",
			true,
			consts.ErrInvalidDocumentCountry,
		},
		{
			"should fail due to string with only spaces",
			"    ",
			true,
			consts.ErrInvalidDocumentCountry,
		},
		{
			"should fail due to exceeded maximum length of string",
			"12345678912345678901234567890123412345678912345678901234567890123",
			true,
			consts.ErrInvalidDocumentCountry,
		},
		{
			"should pass validation",
			"Mexico",
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateCountry(tt.country)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateCountry() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateDistinctPopulation(t *testing.T) {

	tests := []struct {
		name               string
		distinctPopulation string
		wantErr            bool
		expErr             error
	}{
		{
			"should pass on valid normal input",
			"gabon/southwest africa",
			false,
			nil,
		},
		{
			"should pass on valid normal input with leading spaces",
			"   gabon/southwest africa",
			false,
			nil,
		},
		{
			"should pass on valid normal input with trailing spaces",
			"gabon/southwest africa    ",
			false,
			nil,
		},
		{
			"should pass on valid normal input with duplicate spaces",
			"    gabon/southwest     africa    ",
			false,
			nil,
		},
		{
			"should pass on valid normal input with upper cases character",
			"    gaboN/southwest     africa    ",
			false,
			nil,
		},
		{
			"should fail due to an invalid distinct population",
			"gabon/southwest america",
			true,
			consts.ErrInvalidDocumentPopulation,
		},
		{
			"should fail due to an empty string",
			" ",
			true,
			consts.ErrInvalidDocumentPopulation,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateDistinctPopulation(tt.distinctPopulation)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateDistinctPopulation() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateCallTypeName(t *testing.T) {
	tests := []struct {
		name         string
		callTypeName string
		wantErr      bool
		expErr       error
	}{
		{
			"should fail due to an empty string",
			"",
			true,
			consts.ErrInvalidDocumentCallTypeName,
		},
		{
			"should fail due to string with only spaces",
			"    ",
			true,
			consts.ErrInvalidDocumentCallTypeName,
		},
		{
			"should fail due to exceeded maximum length of string",
			"12345678912345678901234567890123412345678912345678901234567890123",
			true,
			consts.ErrInvalidDocumentCallTypeName,
		},
		{
			"should pass validation",
			"Gunshot",
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateCallTypeName(tt.callTypeName)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateCallTypeName() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateDescription(t *testing.T) {
	tests := []struct {
		name        string
		description string
		wantErr     bool
		expErr      error
	}{
		{
			"should pass on empty string",
			"",
			false,
			nil,
		},
		{
			"should pass validation",
			"some description",
			false,
			nil,
		},
		{
			"should pass reading a valid description from text file",
			string(validDocumentDescription),
			false,
			nil,
		},
		{
			"should fail due reading an invalid description from text file",
			string(invalidDocumentDescription),
			true,
			consts.ErrInvalidDescription,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateDescription(tt.description)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateDescription() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateRecordTimestamp(t *testing.T) {
	tests := []struct {
		name      string
		timestamp int64
		wantErr   bool
		expErr    error
	}{
		{
			"should pass on Jan 1, 1990",
			consts.MinTimestamp,
			false,
			nil,
		},
		{
			"should fail before Jan 1, 1990",
			consts.MinTimestamp - 1,
			true,
			consts.ErrInvalidDocumentRecordTimestamp,
		},
		{
			"should fail if it happened in the  future",
			time.Now().Unix() + 5,
			true,
			consts.ErrInvalidDocumentRecordTimestamp,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateRecordTimestamp(tt.timestamp)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateRecordTimestamp() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateCreateTimestamp(t *testing.T) {
	cases := []struct {
		name                 string
		inputCreateTimestamp int64
		inputRecordTimestamp int64
		isExpErr             bool
		expError             error
	}{
		{
			"should pass as document is not created at this point",
			0,
			1514764800,
			false,
			nil,
		},
		{
			"should pass as creation time happened after recorded time",
			1539831496,
			1514764800,
			false,
			nil,
		},
		{
			"should fail as recorded time happened after creation of the document",
			1514764800,
			1539831496,
			true,
			consts.ErrInvalidDocumentCreateTimestamp,
		},
		{
			"should fail as creation time happened in the future",
			time.Now().Unix() + 5,
			1539831496,
			true,
			consts.ErrInvalidDocumentCreateTimestamp,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			err := ValidateCreateTimestamp(c.inputCreateTimestamp, c.inputRecordTimestamp)
			if c.isExpErr {
				assert.EqualError(t, err, c.expError.Error(), c.name)
			}
		})
	}
}

func TestValidateUpdateTimestamp(t *testing.T) {
	cases := []struct {
		name                 string
		inputUpdateTimestamp int64
		inputCreateTimestamp int64
		isExpErr             bool
		expErr               error
	}{
		{
			"should pass as document was never updated",
			0,
			1514764800,
			false,
			nil,
		},
		{
			"should pass as update time happened after creation time",
			1514764801,
			1514764800,
			false,
			nil,
		},
		{
			"should fail update time happened before creation time",
			1514764800,
			1539831496,
			true,
			consts.ErrInvalidUpdateTimestamp,
		},
		{
			"should fail as update time happened in the future time",
			time.Now().Unix() + 5,
			1539831496,
			true,
			consts.ErrInvalidUpdateTimestamp,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			err := ValidateUpdateTimestamp(c.inputUpdateTimestamp, c.inputCreateTimestamp)
			if c.isExpErr {
				assert.EqualError(t, err, c.expErr.Error(), c.name)
			}
		})
	}
}

func TestValidateLatitude(t *testing.T) {
	cases := []struct {
		name     string
		input    float32
		isExpErr bool
		expErr   error
	}{
		{
			"should fail due to below minimum latitude",
			consts.MinLatitude - 1,
			true,
			consts.ErrInvalidDocumentLatitude,
		},
		{
			"should pass on minimum latitude",
			consts.MinLatitude,
			false,
			nil,
		},
		{
			"should pass with zero latitude",
			0,
			false,
			nil,
		},
		{
			"should pass on valid latitude",
			45,
			false,
			nil,
		},
		{
			"should pass on maximum allowed latitude",
			consts.MaxLatitude,
			false,
			nil,
		},
		{
			"should fail due to above maximum latitude",
			consts.MaxLatitude + 1,
			true,
			consts.ErrInvalidDocumentLatitude,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			err := ValidateLatitude(c.input)
			if c.isExpErr {
				assert.EqualError(t, err, c.expErr.Error(), c.name)
			}
		})
	}
}

func TestValidateLongitude(t *testing.T) {
	cases := []struct {
		name     string
		input    float32
		isExpErr bool
		expErr   error
	}{
		{
			"should fail due to below minimum longitude",
			consts.MinLongitude - 1,
			true,
			consts.ErrInvalidDocumentLongitude,
		},
		{
			"should pass on minimum longitude",
			consts.MinLongitude,
			false,
			nil,
		},
		{
			"should pass with zero longitude",
			0,
			false,
			nil,
		},
		{
			"should pass on valid longitude",
			159,
			false,
			nil,
		},
		{
			"should pass on maximum allowed longitude",
			consts.MaxLongitude,
			false,
			nil,
		},
		{
			"should fail due to above maximum longitude",
			consts.MaxLongitude + 1,
			true,
			consts.ErrInvalidDocumentLongitude,
		},
	}
	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			err := ValidateLongitude(c.input)
			if c.isExpErr {
				assert.EqualError(t, err, c.expErr.Error(), c.name)
			}
		})
	}
}

func TestValidateSensorName(t *testing.T) {
	tests := []struct {
		name       string
		sensorName string
		wantErr    bool
		expErr     error
	}{
		{
			"should pass on valid sensor name",
			"Tag",
			false,
			nil,
		},
		{
			"should pass on emtpy string",
			"",
			false,
			nil,
		},
		{
			"should fail due to exceeding maximum string length",
			"12345678912345678901234567890123412345678912345678901234567890123",
			true,
			consts.ErrInvalidDocumentSensorName,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateSensorName(tt.sensorName)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateSensorName() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func Test_isValidMediaMap(t *testing.T) {
	type args struct {
		mediaMap   map[string]string
		mediaRegex *regexp.Regexp
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"should fail due to nil arg mediaMap",
			args{
				nil,
				nil,
			},
			false,
		},
		{
			"should fail due to a bad ID",
			args{
				map[string]string{
					"zff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.jpg",
				},
				nil,
			},
			false,
		},
		{
			"should fail due to an empty URL string",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": " ",
				},
				nil,
			},
			false,
		},
		{
			"should fail due to unsupported audio format",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.jpg",
				},
				audioRegex,
			},
			false,
		},
		{
			"should fail due to missing scheme",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.jpg",
				},
				imageRegex,
			},
			false,
		},
		{
			"should pass with a valid image map",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.jpg",
				},
				imageRegex,
			},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isValidMediaMap(tt.args.mediaMap, tt.args.mediaRegex); got != tt.want {
				t.Errorf("isValidMediaMap() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestValidateAudioMap(t *testing.T) {
	type args struct {
		audioMap map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		expErr  error
	}{
		{
			"should pass with a valid audio map",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.wav",
				},
			},
			false,
			nil,
		},
		{
			"should fail due to unsupported audio format",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.jpg",
				},
			},
			true,
			consts.ErrInvalidDocumentAudioMap,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateAudioMap(tt.args.audioMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateAudioMap() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateImageMap(t *testing.T) {
	type args struct {
		imeageMap map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		expErr  error
	}{
		{
			"should pass with a valid image map",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.jpg",
				},
			},
			false,
			nil,
		},
		{
			"should fail due to unsupported image format",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.wav",
				},
			},
			true,
			consts.ErrInvalidDocumentImageMap,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateImageMap(tt.args.imeageMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateImageMap() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateVideoMap(t *testing.T) {
	type args struct {
		videoMap map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		expErr  error
	}{
		{
			"should pass with a valid video map",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.mp4",
				},
			},
			false,
			nil,
		},
		{
			"should fail due to unsupported video format",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.wav",
				},
			},
			true,
			consts.ErrInvalidDocumentVideoMap,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateVideoMap(tt.args.videoMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateVideoMap() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateFileMap(t *testing.T) {
	type args struct {
		fileMap map[string]string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
		expErr  error
	}{
		{
			"should pass with a valid file map",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "https://hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.mp4",
				},
			},
			false,
			nil,
		},
		{
			"should fail due to invalid URL",
			args{
				map[string]string{
					"aff30392-8ec8-45a4-ba94-5e22c4a686de": "hwscdevstorage.blob.core.windows.net/images/Seger_Conga_CaboMexico_Tag_Acousonde_20140313_112313_8000_3_BreedingMigrating.wav",
				},
			},
			true,
			consts.ErrInvalidDocumentFileMap,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateFileMap(tt.args.fileMap)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateFileMap() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateRequestPage(t *testing.T) {
	tests := []struct {
		name    string
		page    uint32
		wantErr bool
		expErr  error
	}{
		{
			"should return an error due to requested page is 0",
			0,
			true,
			consts.ErrInvalidRequestPage,
		},
		{
			"should return an error due to requested page is more than the allowed max limit",
			consts.DefaultMaxTargetPage + 1,
			true,
			consts.ErrInvalidRequestPage,
		},
		{
			"should pass due to valid page request",
			5,
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateRequestPage(tt.page)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateRequestPage() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}

func TestValidateRequestDocumentsPerPage(t *testing.T) {
	tests := []struct {
		name    string
		page    uint32
		wantErr bool
		expErr  error
	}{
		{
			"should return an error due to requested documents per page is more than the allowed max limit",
			consts.DefaultMaxDocumentsPerPage + 1,
			true,
			consts.ErrInvalidRequestDocumentsPerPage,
		},
		{
			"should pass due to valid documents per page request",
			5,
			false,
			nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := ValidateRequestDocumentsPerPage(tt.page)
			if (err != nil) != tt.wantErr {
				t.Errorf("ValidateRequestDocumentsPerPage() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				assert.EqualError(t, err, tt.expErr.Error(), tt.name)
			}
		})
	}
}
