// Package conf is used to configure our GRPC host and MongoDB reader and writer.
//
// If the environment variables are non-existent, then the default values are used for development and testing.
package conf
