package conf

import (
	"fmt"
	"gitlab.com/hwsc/app/lib/go/conf"
	"gitlab.com/hwsc/app/lib/go/hosts"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
)

var (
	// GRPCHost address and port of GRPC microservice
	GRPCHost = hosts.Host{
		Address: conf.GetEnvOrDefault(consts.KeyDocAddr, consts.DefaultDocSvcAddr),
		Port:    conf.GetEnvOrDefault(consts.KeyDocPort, consts.DefaultDocSvcPort),
		Network: conf.GetEnvOrDefault(consts.KeyDocNetwork, consts.DefaultDocSvcNetwork),
	}

	// DocumentDB represents the Document database
	DocumentDB = hosts.DocumentDBHost{
		Host:           conf.GetEnvOrDefault(consts.KeyDocMongoDBHost, consts.DefaultMongoDBHost),
		Port:           conf.GetEnvOrDefault(consts.KeyDocMongoDBPort, consts.DefaultMongoDBPort),
		Writer:         conf.GetEnvOrDefault(consts.KeyDocMongoDBWriter, consts.DefaultMongoDBWriter),
		WriterPassword: conf.GetEnvOrDefault(consts.KeyDocMongoDBWriterPassword, consts.DefaultMongoDBWriterPassword),
		Reader:         conf.GetEnvOrDefault(consts.KeyDocMongoDBReader, consts.DefaultMongoDBReader),
		ReaderPassword: conf.GetEnvOrDefault(consts.KeyDocMongoDBReaderPassword, consts.DefaultMongoDBReaderPassword),
		Name:           conf.GetEnvOrDefault(consts.KeyDocMongoDBName, consts.DefaultMongoDBName),
		Collection:     conf.GetEnvOrDefault(consts.KeyDocMongoDBCollection, consts.DefaultMongoDBColl),
	}
	// MongoDBConnStr format string for setting up MongoDB connection string
	MongoDBConnStr string
	// MongoDBWriterConnStr MongoDB writer string
	MongoDBWriterConnStr string
	// MongoDBReaderConnStr MongoDB reader string
	MongoDBReaderConnStr string
)

func init() {
	MongoDBConnStr = "mongodb://%s:%s@%s:%s/%s"
	MongoDBWriterConnStr = fmt.Sprintf(
		MongoDBConnStr,
		DocumentDB.Writer,
		DocumentDB.WriterPassword,
		DocumentDB.Host,
		DocumentDB.Port,
		DocumentDB.Name,
	)
	MongoDBReaderConnStr = fmt.Sprintf(
		MongoDBConnStr,
		DocumentDB.Reader,
		DocumentDB.ReaderPassword,
		DocumentDB.Host,
		DocumentDB.Port,
		DocumentDB.Name,
	)
}
