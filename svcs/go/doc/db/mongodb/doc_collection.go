package mongodb

import (
	"context"
	"gitlab.com/hwsc/app/lib/protobufs/svcs/doc"
	"gitlab.com/hwsc/app/svcs/go/doc/conf"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

var (
	_newMongoClient   = NewMongoClient
	_newMongoDatabase = NewMongoDatabase
)

type (
	// DocumentDatabaseInterface specifies the behavior for querying the MongoDB document collection.
	DocumentDatabaseInterface interface {
		// FindDocument finds a document from the MongoDB document collection.
		FindDocument(context.Context, string) (*doc.Document, error)
		// FindUserDocuments over the matching documents in the MongoDB document collection
		FindUserDocuments(context.Context, string, uint32, uint32) ([]*doc.Document, error)
		// CountUserDocuments returns the number of documents in the collection.
		CountUserDocuments(context.Context, string) (uint64, error)
	}

	// DocumentDatabase implements DocumentDatabaseInterface for querying the MongoDB document collection.
	DocumentDatabase struct {
		//DocDB is the wrapper or handle for the database's behaviors.
		DocDB MongoDatabaseInterface
	}
)

// FindDocument finds a document from the MongoDB document collection.
//
// This does not need a filter other than the document identifier
func (d *DocumentDatabase) FindDocument(ctx context.Context, duid string) (*doc.Document, error) {
	objectID, err := primitive.ObjectIDFromHex(duid)
	if err != nil {
		return nil, err
	}
	coll, err := d.DocDB.GetCollection(conf.DocumentDB.Collection)
	if err != nil {
		return nil, err
	}
	document := &doc.Document{}
	filter := bson.M{"_id": objectID}
	if err := coll.FindOne(ctx, filter).Decode(document); err != nil {
		return nil, err
	}
	return document, nil
}

// FindUserDocuments executes a find command and returns a cursor over the matching user's documents in the collection using a target page.
//
// The default documents per page returned is 15 unless specified. The specified documents per page cannot be more than what is allowed by service.go.
//
// Returns documents sorted in descending order using create timestamp.
func (d *DocumentDatabase) FindUserDocuments(ctx context.Context, userID string, page, documentsPerPage uint32) ([]*doc.Document, error) {
	coll, err := d.DocDB.GetCollection(conf.DocumentDB.Collection)
	if err != nil {
		return nil, err
	}
	filter := bson.M{"uuid": userID}
	// 1. descending sort using document's creation time
	// 2. skip documents per page e.g. to get page 1 doc, then do the following:
	//	skip = (page - 1) * num_of_documents_per page, limit = num_of_documents_per page
	documentsPerPageLimit := consts.DefaultDocumentsPerPage
	if documentsPerPage != 0 {
		documentsPerPageLimit = documentsPerPage
	}
	opts := options.
		Find().
		SetSort(bson.D{primitive.E{Key: "createTimestamp", Value: -1}}).
		SetSkip(int64((page - 1) * documentsPerPageLimit)).
		SetLimit(int64(documentsPerPageLimit)).
		SetMaxTime(consts.DefaultMaxAwaitTime)
	cursor := coll.Find(ctx, filter, opts)
	// https://pkg.go.dev/go.mongodb.org/mongo-driver/mongo?tab=doc#Collection.Find
	var documents []*doc.Document
	if err := cursor.All(ctx, &documents); err != nil {
		return nil, err
	}
	return documents, nil
}

// CountUserDocuments returns the number of user's documents in the collection.
func (d *DocumentDatabase) CountUserDocuments(ctx context.Context, userID string) (uint64, error) {
	coll, err := d.DocDB.GetCollection(conf.DocumentDB.Collection)
	if err != nil {
		return 0, err
	}
	filter := bson.M{"uuid": userID}
	// counting all the documents for a user with an index should not have that much performance impact
	countDocumentsOpts := options.
		Count().
		SetMaxTime(consts.DefaultMaxAwaitTime)
	count, err := coll.CountDocuments(ctx, filter, countDocumentsOpts)
	if err != nil {
		return 0, err
	}
	return uint64(count), nil
}

// NewDocumentDatabase returns a handle to MongoDB using a given MongoDB client wrapper.
func NewDocumentDatabase(db MongoDatabaseInterface) (DocumentDatabaseInterface, error) {
	if db == nil {
		return nil, consts.ErrNilMongoDatabase
	}
	return &DocumentDatabase{
		DocDB: db,
	}, nil
}

// NewDocumentCollectionClient returns a client to query the MongoDB document collection database.
func NewDocumentCollectionClient(c *string) (DocumentDatabaseInterface, error) {
	client, err := _newMongoClient(c)
	if err != nil {
		return nil, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		return nil, err
	}
	mongoDB, err := _newMongoDatabase(&conf.DocumentDB.Name, client)
	if err != nil {
		return nil, err
	}
	return NewDocumentDatabase(mongoDB)
}
