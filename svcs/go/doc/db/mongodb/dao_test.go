package mongodb

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/hwsc/app/lib/protobufs/svcs/doc"
	"gitlab.com/hwsc/app/svcs/go/doc/conf"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"testing"
	"time"
)

func TestMongoClient_Connect(t *testing.T) {
	a := assert.New(t)
	t.Log("should make a MongoDB client without an error")
	{
		client, err := NewMongoClient(&conf.MongoDBReaderConnStr)
		if a.NoError(err, "should not return an error creating a MongoDB client") {
			a.NotNil(client, "MongoDB client should not be nil")
			// we do not care if there is an error, as it is handled by the middleware
			_ = client.Connect(context.TODO())
		}
	}
}

func TestMongoClient_GetDatabase(t *testing.T) {
	a := assert.New(t)
	t.Log("should make a MongoDB client without an error")
	{
		client, err := NewMongoClient(&conf.MongoDBReaderConnStr)
		if a.NoError(err, "should return an error creating a MongoDB client") {
			a.NotNil(client, "MongoDB client should not be nil")
			// we do not care what we are returning, as it is handled by the middleware
			_ = client.GetDatabase("")
		}
	}
}

func TestNewMongoClient(t *testing.T) {
	a := assert.New(t)
	t.Log("should make a MongoDB client without an error")
	{
		client, err := NewMongoClient(&conf.MongoDBReaderConnStr)
		if a.NoError(err, "should not return an error creating a MongoDB client") {
			a.NotNil(client, "MongoDB client should not be nil")
		}
	}

	t.Log("should return an error during on MongoDB client create")
	{
		originalNewClient := _newClient
		_newClient = func(opts ...*options.ClientOptions) (client *mongo.Client, err error) {
			return nil, errors.New(`error parsing uri: scheme must be "mongodb" or "mongodb+srv"`)
		}
		badConnStr := ""
		client, err := NewMongoClient(&badConnStr)
		if a.Error(err, "should return an error due to bad connection string") {
			a.Nil(client, "should return a nil MongoDB client")
		}
		_newClient = originalNewClient
	}
}

func TestNewMongoDatabase(t *testing.T) {
	a := assert.New(t)
	goodMongoDBClient, err := NewMongoClient(&conf.MongoDBReaderConnStr)
	if a.NoError(err, "should not return an error setting up the MongoDB client") {
		type args struct {
			database *string
			client   MongoClientInterface
		}
		tests := []struct {
			name    string
			args    args
			wantErr bool
			expErr  error
		}{
			{
				"should fail due to nil database string arg",
				args{
					nil,
					goodMongoDBClient,
				},
				true,
				consts.ErrInvalidMongoDatabaseStr,
			},
			{
				"should fail due to empty database string arg",
				args{
					nil,
					goodMongoDBClient,
				},
				true,
				consts.ErrInvalidMongoDatabaseStr,
			},
			{
				"should fail due to nil MongoDB client arg",
				args{
					&conf.MongoDBReaderConnStr,
					nil,
				},
				true,
				consts.ErrNilMongoDBClient,
			},
			{
				"should return a MongoDB client",
				args{
					&conf.MongoDBReaderConnStr,
					goodMongoDBClient,
				},
				false,
				nil,
			},
		}
		for _, tt := range tests {
			t.Run(tt.name, func(t *testing.T) {
				t.Log(tt.name)
				got, err := NewMongoDatabase(tt.args.database, tt.args.client)
				if tt.wantErr {
					a.EqualError(err, tt.expErr.Error(), "error should be the same for creating MongoDB wrapper")
					a.Nil(got, "should get a nil MongoDB wrapper")
				} else {
					a.NoError(err, "should not return an error creating MongoDB wrapper")
					a.NotNil(got, "MongoDB client wrapper should not be nil")
				}
			})
		}
	}
}

func TestMongoDatabase_Client(t *testing.T) {
	a := assert.New(t)
	t.Log("should generate a wrapper for MongoDB")
	{
		client, err := NewMongoClient(&conf.MongoDBReaderConnStr)
		if a.NoError(err, "should not return an error creating a MongoDB wrapper") {
			a.NotNil(client, "should return MongoDB client")
			mongoDBHandle, err := NewMongoDatabase(&conf.MongoDBWriterConnStr, client)
			if a.NoError(err, "no error generating MongoDB wrapper") {
				if a.NotNil(mongoDBHandle, "MongoDB handle should not be nil") {
					a.NotNil(mongoDBHandle.Client(), "should not return an error getting the MongoDB client")
				}
			}
		}
	}
}

func TestMongoDatabase_GetCollection(t *testing.T) {
	a := assert.New(t)
	client, err := NewMongoClient(&conf.MongoDBReaderConnStr)
	if a.NoError(err, "should not return an error creating a MongoDB wrapper") {
		a.NotNil(client, "should return MongoDB client")
		mongoDBHandle, err := NewMongoDatabase(&conf.MongoDBWriterConnStr, client)
		if a.NoError(err, "no error generating MongoDB wrapper") {

			tests := []struct {
				name     string
				collName string
				wantErr  bool
				expErr   error
			}{
				{
					"should return an error due to invalid collection string",
					"",
					true,
					consts.ErrInvalidMongoDBCollectionStr,
				},
				{
					"should return a MongoDB collection handle",
					conf.DocumentDB.Collection,
					false,
					nil,
				},
			}
			for _, tt := range tests {
				t.Run(tt.name, func(t *testing.T) {
					t.Log(tt.name)
					got, err := mongoDBHandle.GetCollection(tt.collName)
					if tt.wantErr {
						a.EqualError(err, consts.ErrInvalidMongoDBCollectionStr.Error(), "should be the same collection string error")
						a.Nil(got, "should be a nil MongoDB collection handle")
					} else {
						a.NoError(err, "should not return an error creating MongoDB collection handle")
						a.NotNil(got, "MongoDB collection handle should not be nil")
					}
				})
			}
		}
	}
}

func TestSingleResult_Decode(t *testing.T) {
	a := assert.New(t)
	client, err := NewMongoClient(&conf.MongoDBReaderConnStr)
	if a.NoError(err, "should not return an error creating a MongoDB wrapper") {
		a.NotNil(client, "should return MongoDB client")
		mongoDBHandle, err := NewMongoDatabase(&conf.MongoDBWriterConnStr, client)
		if a.NoError(err, "no error generating MongoDB wrapper") {
			mongoDBColl, err := mongoDBHandle.GetCollection(conf.DocumentDB.Collection)
			a.NoError(err, "should not return an error creating MongoDB collection handle")
			if a.NotNil(mongoDBColl, "MongoDB collection handle should not be nil") {
				document := &doc.Document{}
				// do not care about the return type as this is only a wrapper for the middleware to use
				_ = mongoDBColl.FindOne(context.TODO(), bson.D{{}}).Decode(document)
			}
		}
	}
}

func TestCursor_All(t *testing.T) {
	a := assert.New(t)
	t.Log("should return the error from the Cursor wrapper")
	{
		c := &Cursor{c: nil, err: errors.New("")}
		a.Error(c.All(nil, nil), "an error should be returned from the wrapper")
	}
	t.Log("should return a cursor because the wrapper has no error")
	{
		c := &Cursor{c: nil, err: nil}
		// do not care about the return type as this is only a wrapper for the middleware to use
		_ = c.All(nil, nil)
	}
}

type mongoCollectionTestSuite struct {
	suite.Suite
	mongoCollection MongoCollectionInterface
}

func TestMongoCollectionTestSuite(t *testing.T) {
	suite.Run(t, new(mongoCollectionTestSuite))
}

func (suite *mongoCollectionTestSuite) SetupTest() {
	a := assert.New(suite.T())
	client, err := NewMongoClient(&conf.MongoDBReaderConnStr)
	if a.NoError(err, "should not return an error creating a MongoDB wrapper") {
		a.NotNil(client, "should return MongoDB client")
		mongoDBHandle, err := NewMongoDatabase(&conf.MongoDBWriterConnStr, client)
		if a.NoError(err, "no error generating MongoDB wrapper") {
			mongoDBColl, err := mongoDBHandle.GetCollection(conf.DocumentDB.Collection)
			a.NoError(err, "should not return an error creating MongoDB collection handle")
			if a.NotNil(mongoDBColl, "MongoDB collection handle should not be nil") {
				suite.mongoCollection = mongoDBColl
			}
		}
	}
}

func (suite *mongoCollectionTestSuite) TestMongoCollection_FindOne() {
	_ = suite.mongoCollection.FindOne(context.TODO(), bson.D{{}})
}

func (suite *mongoCollectionTestSuite) TestMongoCollection_Find() {
	opts := options.Find().SetSort(bson.D{primitive.E{Key: "createTimestamp", Value: -1}})
	filter := bson.M{"uuid": "dd8b9d8c-165b-4b0a-afe3-f164de2113a2"}
	// do not care about the return type as this is only a wrapper for the middleware to use
	_ = suite.mongoCollection.Find(context.TODO(), filter, opts)
}

func (suite *mongoCollectionTestSuite) TestMongoCollection_CountDocuments() {
	opts := options.Count().SetMaxTime(2 * time.Second)
	// do not care about the return type as this is only a wrapper for the middleware to use
	_, _ = suite.mongoCollection.CountDocuments(context.TODO(), bson.D{{}}, opts)
}
