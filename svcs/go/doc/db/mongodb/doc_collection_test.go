package mongodb

import (
	"context"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gitlab.com/hwsc/app/lib/protobufs/svcs/doc"
	"gitlab.com/hwsc/app/svcs/go/doc/conf"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
	"testing"
)

const (
	validDUID   = "5eb4ad9ffbed2480bb257b01"
	invalidDUID = "5eb4ad9ffbed2480bb257b0z"
)

type mockMongoClient struct {
	mock.Mock
}

func (m *mockMongoClient) Connect(ctx context.Context) error {
	args := m.Called(ctx)
	return args.Error(0)
}

func (m *mockMongoClient) GetDatabase(dbName string) MongoDatabaseInterface {
	args := m.Called(dbName)
	return args.Get(0).(MongoDatabaseInterface)
}

type mockMongoDatabase struct {
	mock.Mock
}

func (m *mockMongoDatabase) Client() MongoClientInterface {
	args := m.Called()
	return args.Get(0).(MongoClientInterface)
}

func (m *mockMongoDatabase) GetCollection(colName string) (MongoCollectionInterface, error) {
	args := m.Called(colName)
	if args.Get(0) != nil {
		return args.Get(0).(MongoCollectionInterface), args.Error(1)
	}
	return nil, args.Error(1)
}

type mockMongoCollection struct {
	mock.Mock
}

func (m *mockMongoCollection) FindOne(ctx context.Context, filter interface{}) SingleResultInterface {
	args := m.Called(ctx, filter)
	return args.Get(0).(SingleResultInterface)
}

func (m *mockMongoCollection) Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) CursorInterface {
	args := m.Called(ctx, filter, opts)
	return args.Get(0).(CursorInterface)
}

func (m *mockMongoCollection) CountDocuments(ctx context.Context, filter interface{}, opts ...*options.CountOptions) (int64, error) {
	args := m.Called(ctx, filter, opts)
	return args.Get(0).(int64), args.Error(1)
}

type mockSingleResult struct {
	mock.Mock
}

func (m *mockSingleResult) Decode(v interface{}) error {
	args := m.Called(v)
	return args.Error(0)
}

type mockCursor struct {
	mock.Mock
}

func (m *mockCursor) All(ctx context.Context, results interface{}) error {
	args := m.Called(ctx, results)
	return args.Error(0)
}

type documentDatabaseFindDocumentTestSuite struct {
	suite.Suite
}

func TestDocumentDatabase_FindDocument(t *testing.T) {
	suite.Run(t, new(documentDatabaseFindDocumentTestSuite))
}

func (suite *documentDatabaseFindDocumentTestSuite) SetupTest() {
}

func (suite *documentDatabaseFindDocumentTestSuite) TestDocumentDatabase_FindDocument_DUID_Input_Error() {
	t := suite.T()
	a := assert.New(t)
	tests := []struct {
		name string
		duid string
	}{
		{
			"should fail for invalid duid",
			invalidDUID,
		},
		{
			"should fail for empty duid",
			"",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mongoDBHandle, err := NewDocumentCollectionClient(&conf.MongoDBReaderConnStr)
			if a.NoError(err, "should return no error for creating a handle to MongoDB") {
				a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
				got, err := mongoDBHandle.FindDocument(context.TODO(), tt.duid)
				if a.Error(err, "should return an error") {
					a.Nil(got, "should return a nil result due to invalid DUID")
				}
			}
		})
	}
}

func (suite *documentDatabaseFindDocumentTestSuite) TestDocumentDatabase_FindDocument_GetCollection_Error() {
	t := suite.T()
	a := assert.New(suite.T())
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(nil, errors.New("")).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		actDoc, err := mongoDBHandle.FindDocument(context.TODO(), validDUID)
		if a.Error(err, "should return an error due to a GetCollection error") {
			a.Nil(actDoc, "should return a nil result due to a GetCollection error")
		}
	}
	mockedMongoDatabase.AssertExpectations(t)
}

func (suite *documentDatabaseFindDocumentTestSuite) TestDocumentDatabase_FindDocument_Decode_Error() {
	t := suite.T()
	a := assert.New(t)
	mockedSingleResult := new(mockSingleResult)
	mockedSingleResult.
		On("Decode", mock.Anything).
		Return(errors.New("")).
		Once()
	mockedMongoCollection := new(mockMongoCollection)
	mockedMongoCollection.
		On("FindOne", mock.Anything, mock.Anything).
		Return(mockedSingleResult).
		Once()
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(mockedMongoCollection, nil).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		actDoc, err := mongoDBHandle.FindDocument(context.TODO(), validDUID)
		if a.Error(err, "should return an error due to decoding issue") {
			a.Nil(actDoc, "should return a nil result due to decoding issue")
		}
	}
	mockedSingleResult.AssertExpectations(t)
	mockedMongoCollection.AssertExpectations(t)
	mockedMongoDatabase.AssertExpectations(t)
}

func (suite *documentDatabaseFindDocumentTestSuite) TestDocumentDatabase_FindDocument_Document() {
	t := suite.T()
	a := assert.New(t)
	expDocument := &doc.Document{
		Duid:               "5e8549ea8be51b42ef24aaa7",
		Uuid:               "dd8b9d8c-165b-4b0a-afe3-f164de2113a2",
		InstrumentType:     "some instrument type",
		SamplingRate:       5000,
		Site:               "some site",
		GroundType:         "some ground type",
		Country:            "USA",
		DistinctPopulation: "some distinct population",
		CallTypeName:       "some call type name",
		Description:        "some description",
		RecordTimestamp:    731155000,
		CreateTimestamp:    731155000,
		UpdateTimestamp:    731155000,
		IsPublic:           true,
		Latitude:           60.5,
		Longitude:          90.1,
		SensorName:         "",
		AudioMap:           make(map[string]string),
		ImageMap:           make(map[string]string),
		VideoMap:           make(map[string]string),
		FileMap:            make(map[string]string),
	}
	objID, _ := primitive.ObjectIDFromHex(expDocument.GetDuid())
	filter := bson.M{"_id": objID}
	mockedSingleResult := new(mockSingleResult)
	mockedSingleResult.
		On("Decode", mock.AnythingOfType("*doc.Document")).
		Return(nil).
		Run(func(args mock.Arguments) {
			arg := args.Get(0).(*doc.Document)
			arg.Duid = expDocument.GetDuid()
			arg.Uuid = expDocument.GetUuid()
			arg.InstrumentType = expDocument.GetInstrumentType()
			arg.SamplingRate = expDocument.GetSamplingRate()
			arg.Site = expDocument.GetSite()
			arg.GroundType = expDocument.GetGroundType()
			arg.Country = expDocument.GetCountry()
			arg.DistinctPopulation = expDocument.GetDistinctPopulation()
			arg.CallTypeName = expDocument.GetCallTypeName()
			arg.Description = expDocument.GetDescription()
			arg.RecordTimestamp = expDocument.GetRecordTimestamp()
			arg.CreateTimestamp = expDocument.GetCreateTimestamp()
			arg.UpdateTimestamp = expDocument.GetUpdateTimestamp()
			arg.IsPublic = expDocument.GetIsPublic()
			arg.Latitude = expDocument.GetLatitude()
			arg.Longitude = expDocument.GetLongitude()
			arg.AudioMap = expDocument.GetAudioMap()
			arg.ImageMap = expDocument.GetImageMap()
			arg.VideoMap = expDocument.GetVideoMap()
			arg.FileMap = expDocument.GetFileMap()
		}).
		Once()
	mockedMongoCollection := new(mockMongoCollection)
	mockedMongoCollection.
		On("FindOne", mock.Anything, filter).
		Return(mockedSingleResult).
		Once()
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(mockedMongoCollection, nil).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		actDoc, err := mongoDBHandle.FindDocument(context.TODO(), expDocument.GetDuid())
		if a.NoError(err, "should not return an error but return a valid document") {
			// GRPC structs do not allow direct comparison of structs
			a.Equal(expDocument.String(), actDoc.String(), "should find the same values with actual fetched document")
		}
	}
	mockedSingleResult.AssertExpectations(t)
	mockedMongoCollection.AssertExpectations(t)
	mockedMongoDatabase.AssertExpectations(t)
}

type documentDatabaseFindUserDocumentsTestSuite struct {
	suite.Suite
	userID string
}

func TestDocumentDatabase_FindUserDocuments(t *testing.T) {
	suite.Run(t, new(documentDatabaseFindUserDocumentsTestSuite))
}

func (suite *documentDatabaseFindUserDocumentsTestSuite) SetupTest() {
	suite.userID = "dd8b9d8c-165b-4b0a-afe3-f164de2113a2"
}

func (suite *documentDatabaseFindUserDocumentsTestSuite) TestDocumentDatabase_FindUserDocuments_GetCollection_Error() {
	t := suite.T()
	a := assert.New(t)
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(nil, errors.New("")).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		res, err := mongoDBHandle.FindUserDocuments(context.TODO(), suite.userID, 1, 0)
		if a.Error(err, "should return an error due to a GetCollection error") {
			a.Nil(res, "should return a nil result due to a GetCollection error")
		}
	}
	mockedMongoDatabase.AssertExpectations(t)
}

func (suite *documentDatabaseFindUserDocumentsTestSuite) TestDocumentDatabase_FindUserDocuments_Cursor_Error() {
	t := suite.T()
	a := assert.New(t)
	mockedCursor := new(mockCursor)
	mockedCursor.
		On("All", mock.Anything, mock.Anything).
		Return(errors.New("")).
		Once()
	mockedMongoCollection := new(mockMongoCollection)
	mockedMongoCollection.
		On("Find", mock.Anything, mock.Anything, mock.Anything).
		Return(mockedCursor).
		Once()
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(mockedMongoCollection, nil).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		res, err := mongoDBHandle.FindUserDocuments(context.TODO(), suite.userID, 1, 0)
		if a.Error(err, "should return an error due to decoding issue") {
			a.Nil(res, "should return a nil result due to decoding issue")
		}
	}
	mockedCursor.AssertExpectations(t)
	mockedMongoCollection.AssertExpectations(t)
	mockedMongoDatabase.AssertExpectations(t)
}

func (suite *documentDatabaseFindUserDocumentsTestSuite) TestDocumentDatabase_FindUserDocuments_Default_Documents_Per_Page() {
	t := suite.T()
	a := assert.New(t)
	mockedCursor := new(mockCursor)
	mockedCursor.
		On("All", mock.Anything, mock.AnythingOfType("*[]*doc.Document")).
		Return(nil).
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*[]*doc.Document)
			fakeDocs := make([]*doc.Document, consts.DefaultDocumentsPerPage)
			for i := range fakeDocs {
				fakeDocs[i] = &doc.Document{}
			}
			*arg = fakeDocs
		}).
		Once()
	mockedMongoCollection := new(mockMongoCollection)
	mockedMongoCollection.
		On("Find", mock.Anything, mock.Anything, mock.Anything).
		Return(mockedCursor).
		Once()
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(mockedMongoCollection, nil).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		res, err := mongoDBHandle.FindUserDocuments(context.TODO(), suite.userID, 1, 0)
		if a.NoError(err, "should not return an error but return valid documents") {
			a.EqualValuesf(consts.DefaultDocumentsPerPage, len(res), "should find %v documents", consts.DefaultDocumentsPerPage)
		}
	}
	mockedCursor.AssertExpectations(t)
	mockedMongoCollection.AssertExpectations(t)
	mockedMongoDatabase.AssertExpectations(t)
}

func (suite *documentDatabaseFindUserDocumentsTestSuite) TestDocumentDatabase_FindUserDocuments_Set_Documents_Per_Page() {
	t := suite.T()
	a := assert.New(t)
	mockedCursor := new(mockCursor)
	mockedCursor.
		On("All", mock.Anything, mock.AnythingOfType("*[]*doc.Document")).
		Return(nil).
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*[]*doc.Document)
			fakeDocs := make([]*doc.Document, consts.DefaultMaxDocumentsPerPage)
			for i := range fakeDocs {
				fakeDocs[i] = &doc.Document{}
			}
			*arg = fakeDocs
		}).
		Once()
	mockedMongoCollection := new(mockMongoCollection)
	mockedMongoCollection.
		On("Find", mock.Anything, mock.Anything, mock.Anything).
		Return(mockedCursor).
		Once()
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(mockedMongoCollection, nil).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		res, err := mongoDBHandle.FindUserDocuments(context.TODO(), suite.userID, 1, consts.DefaultMaxDocumentsPerPage)
		if a.NoError(err, "should not return an error but return valid documents") {
			a.EqualValuesf(consts.DefaultMaxDocumentsPerPage, len(res), "should find %v documents", consts.DefaultMaxDocumentsPerPage)
		}
	}
	mockedCursor.AssertExpectations(t)
	mockedMongoCollection.AssertExpectations(t)
	mockedMongoDatabase.AssertExpectations(t)
}

type documentDatabaseCountUserDocumentsSuite struct {
	suite.Suite
	userID string
}

func TestDocumentDatabase_CountUserDocuments(t *testing.T) {
	suite.Run(t, new(documentDatabaseCountUserDocumentsSuite))
}

func (suite *documentDatabaseCountUserDocumentsSuite) SetupTest() {
	suite.userID = "dd8b9d8c-165b-4b0a-afe3-f164de2113a2"
}

func (suite *documentDatabaseCountUserDocumentsSuite) TestDocumentDatabase_CountUserDocuments_GetCollection_Error() {
	t := suite.T()
	a := assert.New(t)
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(nil, errors.New("")).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		actCount, err := mongoDBHandle.CountUserDocuments(context.TODO(), suite.userID)
		if a.Error(err, "should return an error due to a GetCollection error") {
			a.Zero(actCount, "should return a zero count result due to a GetCollection error")
		}
	}
	mockedMongoDatabase.AssertExpectations(t)
}

func (suite *documentDatabaseCountUserDocumentsSuite) TestDocumentDatabase_CountUserDocuments_CountDocuments_Error() {
	t := suite.T()
	a := assert.New(t)
	mockedMongoCollection := new(mockMongoCollection)
	mockedMongoCollection.
		On("CountDocuments", mock.Anything, mock.Anything, mock.Anything).
		Return(int64(0), errors.New("")).
		Once()
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(mockedMongoCollection, nil).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		actCount, err := mongoDBHandle.CountUserDocuments(context.TODO(), suite.userID)
		if a.Error(err, "should return an error due to CountDocuments issue") {
			a.Zero(actCount, "should return a zero count result due to a CountDocuments error")
		}
	}
	mockedMongoCollection.AssertExpectations(t)
	mockedMongoDatabase.AssertExpectations(t)
}

func (suite *documentDatabaseCountUserDocumentsSuite) TestDocumentDatabase_CountUserDocuments_CountDocuments_Success() {
	t := suite.T()
	a := assert.New(t)
	var expCount int64 = 1500
	mockedMongoCollection := new(mockMongoCollection)
	mockedMongoCollection.
		On("CountDocuments", mock.Anything, mock.Anything, mock.Anything).
		Return(expCount, nil).
		Once()
	mockedMongoDatabase := new(mockMongoDatabase)
	mockedMongoDatabase.
		On("GetCollection", mock.Anything).
		Return(mockedMongoCollection, nil).
		Once()
	mongoDBHandle, err := NewDocumentDatabase(mockedMongoDatabase)
	if a.NoError(err, "should return no error for creating a handle to MongoDB") {
		a.NotNil(mongoDBHandle, "should return a MongoDB handle for querying")
		actCount, err := mongoDBHandle.CountUserDocuments(context.TODO(), suite.userID)
		if a.NoError(err, "should return no error") {
			a.EqualValues(expCount, actCount, "should be the expected count")
		}
	}
	mockedMongoCollection.AssertExpectations(t)
	mockedMongoDatabase.AssertExpectations(t)
}

func TestNewDocumentDatabase(t *testing.T) {
	a := assert.New(t)
	t.Log("should return a handle to MongoDB")
	{
		client, err := NewMongoClient(&conf.MongoDBReaderConnStr)
		if a.NoError(err, "MongoDB client should be made") {
			mongoDB, err := NewMongoDatabase(&conf.DocumentDB.Name, client)
			if a.NoError(err, "should have no error creating the MongoDB client wrapper") {
				mongoDBHandle, err := NewDocumentDatabase(mongoDB)
				if a.NoError(err, "should have no error creating the MongoDB handle") {
					a.NotNil(mongoDBHandle)
				}
			}
		}
	}

	t.Log("should return an error for passing a nil MongoDB wrapper")
	{
		mongoDBHandle, err := NewDocumentDatabase(nil)
		if a.EqualError(err, consts.ErrNilMongoDatabase.Error(), "should have the same error for nil MongoDB wrapper") {
			a.Nil(mongoDBHandle, "should return a nil MongoDB handle")
		}
	}
}

func TestNewDocumentCollectionClient(t *testing.T) {
	a := assert.New(t)
	t.Log("should not initialize MongoDB document collection client due to an issue creating a MongoDB client")
	{
		originalNewMongoClient := _newMongoClient
		_newMongoClient = func(uri *string) (MongoClientInterface, error) {
			return nil, errors.New("")
		}
		client, err := NewDocumentCollectionClient(&conf.MongoDBReaderConnStr)
		if a.Error(err, "should return an error creating a MongoDB document collection client") {
			a.Nil(client, "should return a nil MongoDB document collection client")
		}
		_newMongoClient = originalNewMongoClient
	}

	t.Log("should not initialize MongoDB document collection client due to MongoDB client connect issue")
	{
		mockedMongoClient := new(mockMongoClient)
		mockedMongoClient.
			On("Connect", mock.Anything).
			Return(errors.New("")).
			Once()
		originalNewMongoClient := _newMongoClient
		_newMongoClient = func(uri *string) (MongoClientInterface, error) {
			return mockedMongoClient, nil
		}
		client, err := NewDocumentCollectionClient(&conf.MongoDBReaderConnStr)
		if a.Error(err, "should return an error creating a MongoDB document collection client") {
			a.Nil(client, "should return a nil MongoDB document collection client")
			mockedMongoClient.AssertExpectations(t)
		}
		_newMongoClient = originalNewMongoClient
	}

	t.Log("should not initialize MongoDB document collection client due to an error in creating a MongoDB client wrapper")
	{
		originalNewMongoDatabase := _newMongoDatabase
		_newMongoDatabase =
			func(database *string, client MongoClientInterface) (MongoDatabaseInterface, error) {
				return nil, errors.New("")
			}
		client, err := NewDocumentCollectionClient(&conf.MongoDBReaderConnStr)
		if a.Error(err, "should return an error creating a MongoDB document collection client") {
			a.Nil(client, "should return a nil MongoDB document collection client")
		}
		_newMongoDatabase = originalNewMongoDatabase
	}

	t.Log("should initialize MongoDB document collection reader and writer")
	{
		client, err := NewDocumentCollectionClient(&conf.MongoDBReaderConnStr)
		if a.NoError(err, "should not return an error creating a MongoDB document collection client") {
			a.NotNil(client, "should not return a nil MongoDB document collection client")
		}
	}
}
