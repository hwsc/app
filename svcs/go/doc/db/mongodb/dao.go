package mongodb

import (
	"context"
	"gitlab.com/hwsc/app/svcs/go/doc/consts"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"strings"
)

var (
	_newClient = mongo.NewClient
)

type (
	// MongoClientInterface specifies the client's behaviors.
	MongoClientInterface interface {
		// Connect the client to MongoDB using a context from the request.
		Connect(context.Context) error
		// GetDatabase returns database handle for the collection to query.
		GetDatabase(string) MongoDatabaseInterface
	}

	// MongoClient implements the MongoClientInterface as a wrapper to MongoDB client.
	MongoClient struct {
		cl *mongo.Client
	}
)

// Connect the client to MongoDB using a context from the request.
func (mc *MongoClient) Connect(ctx context.Context) error {
	// This really does not work with passing request contexts.
	// https://jira.mongodb.org/browse/GODRIVER-1031
	// https://jira.mongodb.org/browse/GODRIVER-979
	return mc.cl.Connect(ctx)
}

// GetDatabase returns the handle for the database that has the collection to query.
func (mc *MongoClient) GetDatabase(dbName string) MongoDatabaseInterface {
	db := mc.cl.Database(dbName)
	return &MongoDatabase{db: db}
}

// NewMongoClient returns a MongoDB client from the given connection string.
func NewMongoClient(uri *string) (MongoClientInterface, error) {
	client, err := _newClient(options.Client().ApplyURI(*uri))
	if err != nil {
		return nil, err
	}
	return &MongoClient{cl: client}, err
}

type (
	// MongoDatabaseInterface specifies the database's behaviors.
	MongoDatabaseInterface interface {
		// Client returns the client used for generating the database handle.
		Client() MongoClientInterface
		// Collection returns a handle to the collection within the database.
		GetCollection(name string) (MongoCollectionInterface, error)
	}

	// MongoDatabase implements the MongoDatabaseInterface as a wrapper to MongoDB database from the client.
	MongoDatabase struct {
		db *mongo.Database
	}
)

// NewMongoDatabase returns the handle to a database from the client.
func NewMongoDatabase(database *string, client MongoClientInterface) (MongoDatabaseInterface, error) {
	if database == nil || strings.TrimSpace(*database) == "" {
		return nil, consts.ErrInvalidMongoDatabaseStr
	}
	if client == nil {
		return nil, consts.ErrNilMongoDBClient
	}
	return client.GetDatabase(*database), nil
}

// Client returns the client used for generating the database handle.
func (md *MongoDatabase) Client() MongoClientInterface {
	client := md.db.Client()
	return &MongoClient{cl: client}
}

// GetCollection returns a handle to the collection within the database.
func (md *MongoDatabase) GetCollection(collName string) (MongoCollectionInterface, error) {
	if strings.TrimSpace(collName) == "" {
		return nil, consts.ErrInvalidMongoDBCollectionStr
	}
	collection := md.db.Collection(collName)
	return &MongoCollection{coll: collection}, nil
}

type (
	// MongoCollectionInterface specifies the CRUD operations to perform against the MongoDB collection.
	MongoCollectionInterface interface {
		// FindOne finds a document from the MongoDB collection.
		FindOne(context.Context, interface{}) SingleResultInterface

		// Find executes a find command and returns a cursor over the matching documents in the collection.
		// The filter parameter must be a document containing query operators and can be used to select which documents
		// are included in the result. It cannot be nil. An empty document (e.g. bson.D{}) should be used to include all documents.
		//
		// The opts parameter can be used to specify options for the operation (see the options.FindOptions documentation).
		//
		// For more information about the command, see https://docs.mongodb.com/manual/reference/command/find/.
		Find(context.Context, interface{}, ...*options.FindOptions) CursorInterface

		// CountDocuments returns the number of documents in the collection.
		//
		// The filter parameter must be a document and can be used to select which documents contribute to the count.
		// It cannot be nil. An empty document (e.g. bson.D{}) should be used to count all documents in the collection.
		// This will result in a full collection scan.
		CountDocuments(context.Context, interface{}, ...*options.CountOptions) (int64, error)
		// TODO: Add more CRUD operations
	}

	// MongoCollection implements the MongoCollectionInterface as a wrapper to MongoDB collection for making queries.
	MongoCollection struct {
		coll *mongo.Collection
	}
)

type (
	// SingleResultInterface provides the expected behavior for decoding MongoDB query.
	SingleResultInterface interface {
		// Decode the result from the query.
		Decode(interface{}) error
	}

	// SingleResult implements SingleResultInterface as a wrapper to the result from the query.
	SingleResult struct {
		sr *mongo.SingleResult
	}
)

// Decode the result from the query.
func (sr *SingleResult) Decode(v interface{}) error {
	return sr.sr.Decode(v)
}

type (
	// CursorInterface defines the behavior to iterate over a stream of documents.
	CursorInterface interface {
		// Decode the result from the query.
		All(context.Context, interface{}) error
	}

	// Cursor is used to iterate over a stream of documents.
	// Each document can be decoded into a Go type via the Decode method or accessed as raw BSON via the Current field.
	Cursor struct {
		c *mongo.Cursor
		// err is the wrapper we get from querying the collection
		// this makes the code testable and simplify where to return the error
		// rather than having multiple if statements
		err error
	}
)

// All iterates the cursor and decodes each document into results.
// The results parameter must be a pointer to a slice.
// The slice pointed to by results will be completely overwritten.
// This method will close the cursor after retrieving all documents.
// If the cursor has been iterated, any previously iterated documents will not be included in results.
func (c *Cursor) All(ctx context.Context, results interface{}) error {
	if c.err != nil {
		return c.err
	}
	return c.c.All(ctx, results)
}

// FindOne finds a document from the MongoDB collection.
func (mc *MongoCollection) FindOne(ctx context.Context, filter interface{}) SingleResultInterface {
	singleResult := mc.coll.FindOne(ctx, filter)
	return &SingleResult{sr: singleResult}
}

// Find executes a find command and returns a cursor over the matching documents in the collection.
// The filter parameter must be a document containing query operators and can be used to select which documents
// are included in the result. It cannot be nil. An empty document (e.g. bson.D{}) should be used to include all documents.
//
// The opts parameter can be used to specify options for the operation (see the options.FindOptions documentation).
//
// For more information about the command, see https://docs.mongodb.com/manual/reference/command/find/.
func (mc *MongoCollection) Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) CursorInterface {
	cursor, err := mc.coll.Find(ctx, filter, opts...)
	// pass error to the Cursor, so it can be tested
	return &Cursor{c: cursor, err: err}
}

// CountDocuments returns the number of documents in the collection.
//
// The filter parameter must be a document and can be used to select which documents contribute to the count.
// It cannot be nil. An empty document (e.g. bson.D{}) should be used to count all documents in the collection.
// This will result in a full collection scan.
func (mc *MongoCollection) CountDocuments(ctx context.Context, filter interface{}, opts ...*options.CountOptions) (int64, error) {
	return mc.coll.CountDocuments(ctx, filter, opts...)
}
