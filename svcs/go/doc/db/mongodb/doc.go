// Package mongodb provides two layers for our document service. The dao.go serves a wrapper for our database layer.
// We can utilize the wrapper from dao.go by defining an interface for a specific MongoDB collection.
// An example of this is the doc_collection.go that serves as a layer between dao.go and the API for querying a specific
// MongoDB collection called "docCollection".
//
// In order to mock our calls for unit testing, we have to setup interfaces for the client, database, and collection for
// MongoDB.
package mongodb
