# doc-svc

<!-- TABLE OF CONTENTS -->
## Table of Contents

* [Purpose](#purpose)
* [Getting Started](#getting-started)
* [Guidelines](#guidelines)

<!-- PURPOSE -->
## Purpose
Provides services to CRUD document or file metadata in MongoDB for a specific user.

The API can be found in the [Slab document](https://hwsc.slab.com/posts/document-service-6l24lf4h).

<!-- GETTING STARTED -->
## Getting Started
All the commands are executed from the project's root directory.
```bash
# To scan for secure development using gosec
$ make gosec

# To get dependencies
$ make dep

# To lint the code base
$ make PROJ_PATH="./svcs/go/doc" lint

# To run the test
$ make PROJ_PATH="./svcs/go/doc" test

# To generate code coverage html page in ./svcs/go/doc/public
$ make PROJ_PATH="./svcs/go/doc" page

# To build the image
$ docker-compose -f dockerfiles/docker-compose.yml build doc-svc
```

<!-- GUIDELINES -->
## Guidelines
- Context from a request should be passed all the way to the database layer
- All unit tests are mocked
    - No auto-generated codes for mocking unless approved
- Integration tests should be performed using Go build tags and Docker containers
- All testing must be automated

```mermaid
graph TD
  A[MongoDB]
  B[DAO]
  D['Foo' Collection Handler]
  C['Document' Collection Handler]
  E['Bar' Collection Handler]
  F[GRPC Server]
  G[Client]
  G -->|GetDocument| F
  F -->|Handle business logic and pass context| C
  B -->|Use internal wrapper to query with context| A
  D -->|Use query interface with context| B
  C -->|Use query interface with context| B
  E -->|Use query interface with context| B
```
