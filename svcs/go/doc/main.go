package main

import (
	pb "gitlab.com/hwsc/app/lib/protobufs/svcs/doc"
	"gitlab.com/hwsc/app/svcs/go/doc/conf"
	"gitlab.com/hwsc/app/svcs/go/doc/service"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	lis, err := net.Listen(conf.GRPCHost.Network, conf.GRPCHost.String())
	if err != nil {
		log.Fatal(err)
	}
	s := grpc.NewServer()
	pb.RegisterDocumentServiceServer(s, &service.Service{})
	// TODO: (https://gitlab.com/hwsc/app/-/issues/60) Add logging library with SIEM capability
	log.Print("doc service started at:", conf.GRPCHost.String())
	if err := s.Serve(lis); err != nil {
		log.Fatal("doc service failed to serve: ", err)
	}
}
