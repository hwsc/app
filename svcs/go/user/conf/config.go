package conf

import (
	pblib "github.com/hwsc-org/hwsc-api-blocks/protobuf/lib"
	"github.com/hwsc-org/hwsc-lib/hosts"
)

const (
	environmentVariablePrefix = "hosts"
)

// TODO: grab default values
var (
	// GRPCHost contains server configs grabbed from env vars
	GRPCHost = hosts.Host{
		Address: "",
		Port:    "50052",
		Network: "tcp",
	}

	// UserDB contains user database configs grabbed from env vars
	UserDB = hosts.UserDBHost{
		Host:     "localhost",
		Name:     "test_user_svc",
		User:     "postgres",
		Password: "secret",
		Port:     "5432",
		SSLMode:  "disable",
	}

	// EmailHost contains smtp configs grabbed from env vars
	EmailHost = hosts.SMTPHost{
		Host:     "smtp.gmail.com",
		Port:     "587",
		Username: "hwsc.test@gmail.com",
		Password: "whatever", // TODO: not the real one
	}

	// DummyAccount reads from environment variables, and it is used for creating accounts
	DummyAccount = pblib.User{
		Email:    "hwss2018@outlook.com",
		Password: "yb2TCfF;nuM59;FL",
	}
)
