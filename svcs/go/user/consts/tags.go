package consts

// TODO: Add comments
const (
	// VerifyEmailToken ...
	VerifyEmailToken string = "VerifyEmailToken -"
	// UpdatingUserRowTag ...
	UpdatingUserRowTag string = "UpdateUserRow -"
	// AuthenticateUserTag ...
	AuthenticateUserTag string = "AuthenticateUser -"
	// CreateUserTag ...
	CreateUserTag string = "CreateUser -"
	// DeleteUserTag ...
	DeleteUserTag string = "DeleteUser -"
	// UpdateUserTag ...
	UpdateUserTag string = "UpdateUser -"
	// GetUserTag ...
	GetUserTag string = "GetUser -"
	// UserServiceTag ...
	UserServiceTag string = "User Service -"
	// GetNewAuthTokenTag ...
	GetNewAuthTokenTag string = "GetNewAuthToken -"
	// MakeNewAuthSecret ...
	MakeNewAuthSecret string = "MakeNewAuthSecret -"
	// GetAuthSecret ...
	GetAuthSecret string = "GetAuthSecret -"
	// VerifyAuthToken ...
	VerifyAuthToken string = "VerifyAuthToken -"
	// PSQL ...
	PSQL string = "PSQL -"
)
