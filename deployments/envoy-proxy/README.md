# Overview
`hwsc/envoy-proxy` image is used to proxy requests to backend services

## Make Docker image
1. Change directory to project's root directory
2. Run the commands below:
```bash
$ export IMAGE_TAG=hwsc/envoy-proxy:local
$ export DOCKERFILE_PATH=deployments/envoy-proxy
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile -t ${IMAGE_TAG} .
```

## Changelog
