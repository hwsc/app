#!/usr/bin/env sh

envsubst < /etc/temp/envoy/envoy.template.yaml > /etc/envoy/envoy.yaml
envoy -c /etc/envoy/envoy.yaml
