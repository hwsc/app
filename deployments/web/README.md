# Overview
`hwsc/web` image is used for service web app

This uses yarn to build the web application and nginx to serve the application.

## Make Docker image
1. Change directory to project's root directory
2. Run the commands below:
```bash
$ export IMAGE_TAG=hwsc/web:local
$ export DOCKERFILE_PATH=deployments/web
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile -t ${IMAGE_TAG} .
```

## Changelog
