# Overview
`hwsc/doc-svc` image is used for running document svc

## Make Docker image
1. Change directory to project's root directory
2. Run the commands below:
```bash
$ export IMAGE_TAG=hwsc/doc-svc:local
$ export DOCKERFILE_PATH=deployments/doc/svc
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile -t ${IMAGE_TAG} .
```

## Changelog
