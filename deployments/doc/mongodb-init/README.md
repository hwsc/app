# Overview
`hwsc/doc-svc-mongodb-init` image is used for initializing document service's MongoDB instance.
This runs the migration script and checks if a directory is mounted with JSON files to pre-populate MongoDB with documents for testing and demonstration.

```mermaid
graph TD
  A(doc-svc-mongodb-init container) -->|1.a. on startup, run migration scripts| B(doc-svc-mongodb VM/container)
  A(doc-svc-mongodb-init container) -->|1.b. on update, run migration scripts| B(doc-svc-mongodb VM/container)
  A --> C{4. is it done?}
  A --> F{2. has json files for populating?}
  F -->|3. populate doc-svc-mongodb| B
  C --> |5.a. on success| D[exit 0]
  C --> |5.b. on failure| E[exit non-zero]
```

## Make Docker image
1. Change directory to project's root directory
2. Run the commands below:
```bash
$ export IMAGE_TAG=hwsc/doc-svc-mongodb-init:local
$ export DOCKERFILE_PATH=deployments/doc/mongodb-init
$ docker build -f ${DOCKERFILE_PATH}/Dockerfile -t ${IMAGE_TAG} ./deployments/
```

## Changelog
