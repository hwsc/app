#!/usr/local/bin/python3.8
import bson
import logging
import json
import os
import pymongo
import subprocess
import sys
import bson.json_util

"""
Reads a directory that can be mounted using Docker to insert documents
to doc-svc-mongodb as required for testing or demonstration purposes.

There is a 30 seconds timeout to allow MongoDB to be available and to
relinquish lock to the script.
"""

# Default time is 30 seconds for now.
TIMEOUT = os.getenv(
    "HWSC_INIT_TIMEOUT",
    "30"
)
TIMEOUT_S = int(TIMEOUT)
VERBOSE = os.getenv(
    "HWSC_VERBOSE",
    ""
)

MONGODB_HOST = os.getenv(
    "HWSC_HOST_DOC_MONGODB_HOST",
    "localhost"
)
MONGODB_PORT = os.getenv(
    "HWSC_HOST_DOC_MONGODB_PORT",
    "27017"
)
MONGODB_USER = os.getenv(
    "HWSC_HOST_DOC_MONGODB_WRITER",
    "testDocumentWriter"
)
MONGODB_PASSWORD = os.getenv(
    "HWSC_HOST_DOC_MONGODB_WRITER_PASSWORD",
    "testDocumentPwd"
)
MONGODB_NAME = os.getenv(
    "HWSC_HOST_DOC_MONGODB_NAME",
    "docDB"
)
MONGODB_COLLECTION = os.getenv(
    "HWSC_HOST_DOC_MONGODB_COLL",
    "docCollection"
)
# default to empty string to run all migration scripts
MONGODB_MIGRATE_TO_N = os.getenv(
    "HWSC_MONGODB_MIGRATE_TO_N",
    ""
)

MONGODB_URL = f"mongodb://{MONGODB_USER}:{MONGODB_PASSWORD}@{MONGODB_HOST}:\
{MONGODB_PORT}/{MONGODB_NAME}"

# use this to insert a document
# mount migrations to start migration
MIGRATIONS_DIR = "migrations"
# mount test_fixtures with JSON documents to insert documents
DOCS_DIR = "test_fixtures"

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%m/%d/%Y %I:%M:%S %p'
)

if not os.path.exists(MIGRATIONS_DIR):
    logging.info("skipping migration")
    logging.info(f"done")
    # no need to exit with error status
    sys.exit(0)

if not os.path.isdir(MIGRATIONS_DIR):
    logging.error("mounted migrations directory is not a directory")
    sys.exit(1)

logging.info("running db migration")
migrate_process = None
try:
    # this blocks until timeout is reached
    migrate_process = subprocess.run(
        [
            "migrate",
            "-source",
            f"file://{MIGRATIONS_DIR}",
            "-database",
            f"{MONGODB_URL}",
            "-lock-timeout",
            TIMEOUT,
            "up",
            f"{MONGODB_MIGRATE_TO_N}"
        ],
        capture_output=True,
        check=True,
        encoding="utf-8",
        timeout=TIMEOUT_S)
    if migrate_process.stdout:
        logging.info(migrate_process.stdout.rstrip())
    if migrate_process.stderr:
        logging.info(f"Changes:\n{migrate_process.stderr.rstrip()}")
except subprocess.CalledProcessError as e:
    logging.error(e.stderr.rstrip())
    sys.exit(1)
except subprocess.TimeoutExpired as e:
    logging.error(e)
    sys.exit(1)
except Exception as e:
    logging.error(e)
    logging.error(sys.exc_info()[0])
    sys.exit(1)

if not os.path.exists(DOCS_DIR):
    logging.info("skipping insertion")
    # no need to exit with error status
    sys.exit(0)

if not os.path.isdir(DOCS_DIR):
    logging.error("mounted document directory is not a directory")
    sys.exit(1)

logging.info("initiating insertion")
client = pymongo.MongoClient(
    MONGODB_URL,
    connectTimeoutMS=TIMEOUT_S*1000,
    socketTimeoutMS=TIMEOUT_S*1000
)
database = client["docDB"]
collection = database[MONGODB_COLLECTION]


def object_id_handler(d):
    res = {}
    for key in d.keys():
        if key == "_id":
            objID = bson.objectid.ObjectId(d["_id"])
            res["_id"] = objID
        else:
            res[key] = d[key]
    return res


count = 0
for filename in os.listdir(DOCS_DIR):
    with open(os.path.join(DOCS_DIR, filename), "r") as file:
        try:
            json_list = json.load(
                file,
                object_hook=object_id_handler,
                parse_int=lambda l: bson.Int64(l)  # parse int to bson long
            )
            if isinstance(json_list, list):
                for json_object in json_list:
                    if VERBOSE:
                        logging.info(bson.json_util.dumps(
                            json_object, indent=4))
                    res = collection.insert_one(json_object)
                    logging.info(f"inserted doc with id: {res.inserted_id}")
                    count += 1
            else:
                logging.error("unsupported: file with a list of objects only")
        except (json.JSONDecodeError, pymongo.errors.WriteError) as e:
            logging.error(f"{e} from file: {filename}")
            sys.exit(1)
        except Exception as e:
            logging.error(e)
            logging.error(sys.exc_info()[0])
            sys.exit(1)

logging.info(f"inserted {count} entries")
logging.info("done")
