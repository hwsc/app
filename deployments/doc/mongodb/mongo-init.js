db.auth('mongoadmin', 'secret');

db = db.getSiblingDB('docDB')

db.createUser({
    user: 'testDocumentWriter',
    pwd: 'testDocumentPwd',
    roles: [
        {
            role: 'readWrite',
            db: 'docDB',
        },
    ],
});

db.createUser({
    user: 'testDocumentReader',
    pwd: 'testDocumentPwd',
    roles: [
        {
            role: 'read',
            db: 'docDB',
        },
    ],
});
