package conf

import "os"

// GetEnvOrDefault helper function to read an environment or return a default value
func GetEnvOrDefault(k string, dv string) string {
	if v, ok := os.LookupEnv(k); ok {
		return v
	}
	return dv
}
