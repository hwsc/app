package conf

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestGetEnvOrDefault(t *testing.T) {
	t.Log("Should load environment variable")
	{
		k := "foo"
		expVal := "bar"
		if err := os.Setenv(k, expVal); err != nil {
			assert.NoError(t, err, "no error setting env")
		}
		actVal := GetEnvOrDefault(k, "")
		assert.Equal(t, expVal, actVal, "should get expected value from the env")
		if err := os.Unsetenv(k); err != nil {
			assert.NoError(t, err, "no error unsetting env")
		}
	}
	t.Log("Should load default value due missing environment variable")
	{
		expVal := "baz"
		actVal := GetEnvOrDefault("bar", expVal)
		assert.Equal(t, expVal, actVal, "should get default value instead")
	}
}
