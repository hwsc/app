package consts

import "errors"

var (
	// ErrNilInterface is error for nil interface
	ErrNilInterface = errors.New("nil interface")
	// ErrEmptyString is error for empty string
	ErrEmptyString = errors.New("empty string")
	// ErrNilIdentification is error for nil identification
	ErrNilIdentification = errors.New("nil identification")
	// ErrNilHeader is error for nil header
	ErrNilHeader = errors.New("nil header")
	// ErrNilBody is error for nil body
	ErrNilBody = errors.New("nil body")
	// ErrNilSecret is error for nil secret
	ErrNilSecret = errors.New("nil secret")
	// ErrExpiredBody is error for expired token
	ErrExpiredBody = errors.New("expired token")
	// ErrEmptyToken is error for empty token string
	ErrEmptyToken = errors.New("empty token string")
	// ErrEmptySecret is error for empty secret key
	ErrEmptySecret = errors.New("empty secret key")
	// ErrExpiredSecret is error for expired secret key
	ErrExpiredSecret = errors.New("expired secret key")
	// ErrInvalidSecretCreateTimestamp is error for invalid secret create timestamp
	ErrInvalidSecretCreateTimestamp = errors.New("invalid secret create timestamp")
	// ErrIncompleteToken is error for token missing header, body or/and signature
	ErrIncompleteToken = errors.New("token should contain header, body, signature")
	// ErrInvalidSignature is error for invalid signature
	ErrInvalidSignature = errors.New("invalid signature")
	// ErrInvalidPermission is error for unauthorized permission
	ErrInvalidPermission = errors.New("unauthorized permission")
	// ErrInvalidUUID is error for invalid uuid
	ErrInvalidUUID = errors.New("invalid uuid")
	// ErrInvalidDUID is error for invalid duid
	ErrInvalidDUID = errors.New("invalid duid")
	// ErrInvalidEncodedHeader is error for invalid encoded header
	ErrInvalidEncodedHeader = errors.New("invalid encoded header")
	// ErrInvalidEncodedBody is error for invalid encoded body
	ErrInvalidEncodedBody = errors.New("invalid encoded body")
	// ErrInvalidSignatureValue is error for invalid signature value
	ErrInvalidSignatureValue = errors.New("invalid signature value")
	// ErrNoHashAlgorithm is error for missing hashing algorithm
	ErrNoHashAlgorithm = errors.New("no hashing algorithm")
	// ErrInvalidRequiredTokenType is error for invalid required token type
	ErrInvalidRequiredTokenType = errors.New("invalid required token type")
	// ErrUnknownTokenType is error for unknown token type
	ErrUnknownTokenType = errors.New("unknown token type")
	// ErrUnknownAlgorithm is error for unknown algorithm
	ErrUnknownAlgorithm = errors.New("unknown algorithm")
	// ErrUnknownPermission is error for unknown permission
	ErrUnknownPermission = errors.New("unknown permission")
	// ErrInvalidTokenSize is error for invalid token size
	ErrInvalidTokenSize = errors.New("invalid token size")
	// ErrInvalidTimeStamp is error for zero timestamp
	ErrInvalidTimeStamp = errors.New("zero timestamp")
	// ErrInvalidNumberOfDays is error for invalid number of days to add to expiration
	ErrInvalidNumberOfDays = errors.New("invalid number of days to add to expiration timestamp")
)
