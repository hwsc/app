import React from 'react';
import styled from 'styled-components/macro';
import MenuTopBar from './MenuTopBar';

export default {
  title: 'Menu',
};

export const TopBar = () => <MenuTopBar />;

export const StyledTopBar = () => (
  <Container>
    <MenuTopBar />
  </Container>
);

const Container = styled.div`
  border-bottom: 1px solid ${(props) => props.theme.colors.borderLight};
`;
