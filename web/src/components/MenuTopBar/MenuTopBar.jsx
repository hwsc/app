import React from 'react';
import styled from 'styled-components/macro';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const MenuTopBar = () => {
  return (
    <TopBar>
      <span>
        <FontAwesomeIcon icon="user-circle" />
      </span>
    </TopBar>
  );
};

const TopBar = styled.div`
  padding: 8px;
  text-align: right;

  span {
    font-size: 34px;
  }
`;

export default MenuTopBar;
