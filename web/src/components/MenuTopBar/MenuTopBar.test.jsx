import React from 'react';
import { render } from 'utils/testing';
import MenuTopBar from './MenuTopBar';

test('render', () => {
  const { container } = render(<MenuTopBar />);
  expect(container.firstChild).toMatchSnapshot();
});
