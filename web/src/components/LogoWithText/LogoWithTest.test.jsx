import React from 'react';
import { render } from 'utils/testing';
import LogoWithText from './LogoWithText';
import { MemoryRouter } from 'react-router-dom';

test('render', () => {
  const { container } = render(
    <MemoryRouter>
      <LogoWithText />
    </MemoryRouter>
  );
  expect(container.firstChild).toMatchSnapshot();
});
