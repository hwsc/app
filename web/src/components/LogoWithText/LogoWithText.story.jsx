import React from 'react';
import LogoWithText from './LogoWithText';

export default {
  title: 'Logo',
};

export const BlackLogoWithText = () => <LogoWithText />;
