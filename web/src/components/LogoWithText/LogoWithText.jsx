import React from 'react';
import styled from 'styled-components/macro';
import { NavLink } from 'react-router-dom';
import { routePath } from 'config';
import Logo from 'components/Logo';

const LogoWithText = () => (
  <LogoContainer>
    <NavLink to={routePath.home}>
      <Logo />
      <h4>HWSC</h4>
    </NavLink>
  </LogoContainer>
);

const LogoContainer = styled.div`
  text-align: center;
  padding: 20px;
  display: flex;
  justify-content: center;

  a {
    text-decoration: none;
    color: ${(props) => props.theme.colors.textMain};
    display: flex;
  }

  img {
    margin-right: 10px;
  }
`;

export default LogoWithText;
