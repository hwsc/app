import React from 'react';
import styled from 'styled-components/macro';
import MenuSideBar from './MenuSideBar';

export default {
  title: 'Menu',
};

export const SideBar = () => <MenuSideBar />;

export const StyledSideBar = () => (
  <Container>
    <MenuSideBar />
  </Container>
);

const Container = styled.div`
  width: 200px;
`;
