import React from 'react';
import styled from 'styled-components/macro';
import NavLinkButton from 'components/NavLinkButton';
import LogoWithText from 'components/LogoWithText';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { routePath } from 'config';

const MenuSideBar = () => {
  return (
    <SideBar>
      <LogoWithText />
      <nav>
        <ul>
          <li>
            <NavLinkButton to={routePath.myData}>
              <span>
                <FontAwesomeIcon icon="folder" />
              </span>
              My Data
            </NavLinkButton>
          </li>
          <li>
            <NavLinkButton to={routePath.uploadFiles}>
              <span>
                <FontAwesomeIcon icon="cloud-upload-alt" />
              </span>
              Upload Files
            </NavLinkButton>
          </li>
          <li>
            <NavLinkButton to={routePath.sharedData}>
              <span>
                <FontAwesomeIcon icon="globe" />
              </span>
              Shared Data
            </NavLinkButton>
          </li>
          <li>
            <NavLinkButton to={routePath.callTypes}>
              <span>
                <FontAwesomeIcon icon={['fab', 'soundcloud']} />
              </span>
              Call Types
            </NavLinkButton>
          </li>
        </ul>
      </nav>
    </SideBar>
  );
};

const SideBar = styled.div`
  ul {
    list-style: none;
    margin: 0;
    padding: 0;
  }

  li {
    font-size: 14px;
    width: 100%;
  }

  li:hover {
    background-color: ${(props) => props.theme.colors.btnHover};
  }

  li span {
    font-size: 18px;
    width: 40px;
    display: inline-block;
    text-align: center;
    margin-right: 5px;
  }

  button {
    padding: 18px 0 18px 16px;
    width: 100%;
    text-decoration: none;
    color: ${(props) => props.theme.colors.btnText};
    text-align: left;
    border-left: 5px solid transparent;
  }

  button.active {
    background-color: #ececec;
    border-left: 5px solid #313d4b;
    color: #272727;
  }
`;

export default MenuSideBar;
