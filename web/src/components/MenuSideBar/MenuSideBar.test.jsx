import React from 'react';
import { render } from 'utils/testing';
import MenuSideBar from './MenuSideBar';
import { MemoryRouter } from 'react-router-dom';

test('render', () => {
  const { container } = render(
    <MemoryRouter>
      <MenuSideBar />
    </MemoryRouter>
  );
  expect(container.firstChild).toMatchSnapshot();
});
