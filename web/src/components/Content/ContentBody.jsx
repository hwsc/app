import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

const ContentBody = ({ children }) => <Body>{children}</Body>;

ContentBody.propTypes = {
  children: PropTypes.node.isRequired,
};

const Body = styled.main`
  margin-top: 30px;
`;

export default ContentBody;
