import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

const ContentHeader = ({ title }) => (
  <Header>
    <h1>{title}</h1>
  </Header>
);

ContentHeader.propTypes = {
  title: PropTypes.string.isRequired,
};

const Header = styled.header`
  h1 {
    text-transform: capitalize;
    margin: 0;
  }
`;

export default ContentHeader;
