import React from 'react';
import { render } from 'utils/testing';
import ContentHeader from './ContentHeader';

test('render', () => {
  const { container } = render(<ContentHeader title="some title" />);
  expect(container.firstChild).toMatchSnapshot();
});
