import ContentHeader from './ContentHeader';
import ContentBody from './ContentBody';

export { ContentHeader, ContentBody };
