import React from 'react';
import { render } from 'utils/testing';
import ContentBody from './ContentBody';

test('render', () => {
  const { container } = render(<ContentBody>children</ContentBody>);
  expect(container.firstChild).toMatchSnapshot();
});
