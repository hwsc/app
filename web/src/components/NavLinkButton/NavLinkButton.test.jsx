import React from 'react';
import { Router } from 'react-router-dom';
import { createMemoryHistory } from 'history';
import { render, fireEvent } from 'utils/testing';
import NavLinkButton from './NavLinkButton';

test('render', () => {
  const history = createMemoryHistory();
  const { container, getByText } = render(
    <Router history={history}>
      <NavLinkButton to="/some-route">btn test</NavLinkButton>
    </Router>
  );

  expect(container.firstChild).not.toHaveClass('active');

  fireEvent.click(getByText(/btn test/));
  expect(history.location.pathname).toBe('/some-route');
  expect(container.firstChild).toHaveClass('active');
});
