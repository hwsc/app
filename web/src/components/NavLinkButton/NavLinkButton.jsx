import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import styled from 'styled-components/macro';

/**
 * NavLinkButton provides basic navigation in the form of button
 * since react-router-dom does not provide a button link. Adds
 * class "active" to rendered element when it matches current URL.
 *
 * @param to a string representation of the link location
 * @param restOfProp contains router specific props
 */
const NavLinkButton = ({ to, children, ...restOfProp }) => {
  const [isActive, setIsActive] = React.useState(false);

  React.useEffect(() => {
    if (to !== restOfProp.location.pathname) {
      setIsActive(false);
      return;
    }
    setIsActive(true);
  }, [restOfProp.location.pathname, to]);

  return (
    <Button
      type="button"
      onClick={() => restOfProp.history.push(to)}
      className={isActive ? 'active' : ''}
    >
      {children}
    </Button>
  );
};

NavLinkButton.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

const Button = styled.button`
  border: none;
  background: none;
  cursor: pointer;

  :focus {
    outline: none;
  }
`;

export default withRouter(NavLinkButton);
