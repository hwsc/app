import React from 'react';
import { render } from 'utils/testing';
import LayoutDashboard from './LayoutDashboard';

test('render', () => {
  const { container } = render(
    <LayoutDashboard sideBar={<div>sidebar</div>} topBar={<div>topbar</div>}>
      <div>children</div>
    </LayoutDashboard>
  );
  expect(container.firstChild).toMatchSnapshot();
});
