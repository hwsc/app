import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';

const LayoutDashboard = ({ sideBar, topBar, children }) => {
  return (
    <Layout>
      <div className="left-column">{sideBar}</div>
      <div className="right-column">
        <div className="top-bar">{topBar}</div>
        <div className="content-area">{children}</div>
      </div>
    </Layout>
  );
};

LayoutDashboard.propTypes = {
  sideBar: PropTypes.node.isRequired,
  topBar: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
};

const Layout = styled.div`
  display: flex;
  height: 100%;

  .left-column {
    width: 200px;
    border-right: 1px solid ${(props) => props.theme.colors.borderLight};
  }

  .right-column {
    width: 100%;
    display: flex;
    flex-direction: column;
  }

  .top-bar {
    box-shadow: 0 1px 0 0 ${(props) => props.theme.colors.borderLight};
  }

  .content-area {
    padding: 30px;
  }
`;

export default LayoutDashboard;
