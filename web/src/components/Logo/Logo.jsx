import React from 'react';

const Logo = () => <img src="logo-black.svg" alt="black logo mascot" />;

export default Logo;
