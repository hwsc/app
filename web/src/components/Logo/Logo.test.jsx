import React from 'react';
import { render } from 'utils/testing';
import Logo from './Logo';

test('render', () => {
  const { container } = render(<Logo />);
  expect(container.firstChild).not.toBeNull();
});
