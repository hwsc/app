/**
 * routePath defines all the possible routes that the app can take
 */
export const routePath = Object.freeze({
  home: '/',
  myData: '/my-data',
  uploadFiles: '/upload-files',
  sharedData: '/shared-data',
  callTypes: '/call-types',
});
