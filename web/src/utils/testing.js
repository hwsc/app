/**
 * Custom render for react-testing-library
 *
 * https://testing-library.com/docs/react-testing-library/setup
 * https://testing-library.com/docs/example-react-router
 */
import React from 'react';
import { render } from '@testing-library/react';
import ThemeProvider from 'theme/ThemeProvider';

const AllTheProviders = ({ children }) => (
  <ThemeProvider>{children}</ThemeProvider>
);

const customRender = (ui, options) =>
  render(ui, { wrapper: AllTheProviders, ...options });

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };
