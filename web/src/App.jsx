import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';
import Dashboard from './views/Dashboard';
import ThemeProvider from 'theme/ThemeProvider';

function App() {
  return (
    <BrowserRouter>
      <ThemeProvider>
        <Switch>
          <Route path="/">
            <Dashboard />
          </Route>
        </Switch>
      </ThemeProvider>
    </BrowserRouter>
  );
}

export default App;
