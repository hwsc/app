import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import MenuSideBar from 'components/MenuSideBar';
import MenuTopBar from 'components/MenuTopBar';
import LayoutDashboard from 'components/LayoutDashboard';
import ContentMyData from 'views/ContentMyData';
import ContentUploadFiles from 'views/ContentUploadFiles';
import { routePath } from 'config';

const Dashboard = () => {
  return (
    <LayoutDashboard sideBar={<MenuSideBar />} topBar={<MenuTopBar />}>
      <Switch>
        <Redirect exact from="/" to={routePath.myData} />
        <Route exact path={routePath.myData}>
          <ContentMyData />
        </Route>
        <Route exact path={routePath.uploadFiles}>
          <ContentUploadFiles />
        </Route>
      </Switch>
    </LayoutDashboard>
  );
};

export default Dashboard;
