import React from 'react';
import { ContentHeader, ContentBody } from 'components/Content';

const ContentUploadFiles = () => {
  return (
    <>
      <ContentHeader title="Upload Files" />
      <ContentBody>
        https://www.figma.com/file/0xoz17B7OFY5wxBuTJMH79/prototype_v1?node-id=9%3A259
      </ContentBody>
    </>
  );
};

export default ContentUploadFiles;
