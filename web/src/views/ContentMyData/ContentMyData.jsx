import React from 'react';
import { ContentHeader, ContentBody } from 'components/Content';

const ContentMyData = () => {
  return (
    <>
      <ContentHeader title="My Data" />
      <ContentBody>insert table here</ContentBody>
    </>
  );
};

export default ContentMyData;
