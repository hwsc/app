import React from 'react';
import { render } from 'utils/testing';
import ThemeProvider from './ThemeProvider';

test('children render', () => {
  const { container } = render(
    <ThemeProvider>
      <div>
        <p>test children</p>
        <p>are rendered</p>
      </div>
    </ThemeProvider>
  );

  expect(container.firstChild).toMatchSnapshot();
});
