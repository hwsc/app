/**
 * theme enforces consistent use of styling
 * and avoids duplication and ease of change (single source)
 *
 * Use: for now, colors that you find yourself using in more
 * than one location
 */
const theme = {
  colors: {
    textMain: '#42424b',
    borderLight: '#e3e3e3',
    btnText: '#4d4d4d',
    btnHover: '#f8f8f8',
  },
};

export default theme;
