import React from 'react';
import PropTypes from 'prop-types';
import { ThemeProvider as Theme } from 'styled-components/macro';
import { createGlobalStyle } from 'styled-components/macro';
import theme from './theme';
import './icons';
import './index.css';

/**
 * ThemeProvider serves to avoid importing theme from libs for every use.
 */
const ThemeProvider = ({ children }) => (
  <Theme theme={theme}>
    <GlobalStyle />
    {children}
  </Theme>
);

ThemeProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

const GlobalStyle = createGlobalStyle`
  html, body, #root {
    height: 100%;
  }

  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
      sans-serif;
    color: ${(props) => props.theme.colors.textMain};
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
      monospace;
  }
`;

export default ThemeProvider;
