/**
 * Icons used throughout the app from Font Awesome.
 * https://fontawesome.com/
 *
 * To add: Import name of icon and add it to library.
 * Examples of use in a file:
 *
 *   - eg: faCloudUploadAlt (Solid Style is default)
 *     - import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
 *     - <FontAwesomeIcon icon="cloud-upload-alt" />
 *
 *   - eg: faSoundcloud (Brand style, so we need to add 'fab' keyword)
 *     - import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
 *     - <FontAwesomeIcon icon={['fab', 'soundcloud']} />
 */
import { library } from '@fortawesome/fontawesome-svg-core';
import { faSoundcloud } from '@fortawesome/free-brands-svg-icons';
import {
  faFolder,
  faCloudUploadAlt,
  faGlobe,
  faUserCircle,
} from '@fortawesome/free-solid-svg-icons';

library.add(faSoundcloud, faFolder, faCloudUploadAlt, faGlobe, faUserCircle);
