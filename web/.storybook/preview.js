import React from 'react';
import { addDecorator } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';
import ThemeProvider from '../src/theme/ThemeProvider';

addDecorator((storyFn) => (
  <MemoryRouter>
    <ThemeProvider>{storyFn()}</ThemeProvider>
  </MemoryRouter>
));
