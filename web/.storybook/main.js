module.exports = {
  stories: ['../src/**/*.story.{js,jsx}'],
  addons: [
    '@storybook/preset-create-react-app',
    '@storybook/addon-actions',
    '@storybook/addon-links',
  ],
};
