# HWSC Application

## Table of Contents

- [HWSC Application](#hwsc-application)
  - [Table of Contents](#table-of-contents)
  - [Purpose](#purpose)
  - [Documentation](#documentation)
    - [godoc](#godoc)
  - [Getting Started](#getting-started)
    - [Running/Stopping the application](#runningstopping-the-application)
      - [Checking containers](#checking-containers)
  - [Development](#development)
    - [Mount current working directory](#mount-current-working-directory)

## Purpose
A monorepo of codes for building the HWSC web app.

## Documentation

### godoc
```bash
# install godocs as required (do not use apt installer)
$ go get golang.org/x/tools/cmd/godoc

# run godoc
$ godoc

# visit http://localhost:6060/pkg/gitlab.com/hwsc/app/
```

## Getting Started
This allows to run and test the entire application using Docker compose.

### Running/Stopping the application
> NOTE: This is a work in progress.
>
> `$ docker-compose -f deployments/docker-compose.yml up -d`
>
> Open browser to http://localhost/

```bash
# Use project's root directory path
docker-compose -f deployments/docker-compose.yml up -d
docker-compose -f deployments/docker-compose.yml down -d
```

#### Checking containers
```bash
# Check for running containers
$ docker ps
# Follow the container's logs
$ docker logs -f <container name>
```

## Development
Prior to development, ensure you have read the [on-boarding document](https://hwsc.slab.com/posts/on-boarding-tjy2iks0).

The [hwsc/dev](https://hub.docker.com/repository/docker/hwsc/dev) Docker image can be utilized for development and tooling.

### Mount current working directory
```bash
$ docker run -it --rm -v $PWD:/app -w /app hwsc/dev:<tag> bash
# compile protocol buffers if necessary
$ bash tools/generate_proto.sh
$ exit
# changes are reflected in user's workspace
```
