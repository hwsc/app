#!/bin/bash

# This should be run from the project's root directory only.
# The hwsc/dec Docker image can be used to run this script.
# $ docker run -it --rm -v $PWD:/app -w /app hwsc/dev:<tag> bash
# $ bash tools/generate_proto.sh


# Fail-fast
set -euo pipefail
shopt -s inherit_errexit

PROTOC_VERSION=$(protoc --version | sed 's/[^0-9.]//g')
CURRENT_VERSION="3.12.2"

if [ "$PROTOC_VERSION" != "$CURRENT_VERSION" ]; then
  echo "Please upgrade your protoc version to ${CURRENT_VERSION}"
  exit 1
fi

LIB_ROOT="./lib/protobufs/lib"
USER_ROOT="./lib/protobufs/svcs/user"
DOC_ROOT="./lib/protobufs/svcs/doc"

# use proto_set for making grpcurl
make_proto_set() {
  protoc --proto_path=./lib \
    --descriptor_set_out="$1.protoset" \
    --include_imports \
    "$1.proto"
}

make_pb_go() {
  protoc --go_out=plugins=grpc,paths=source_relative:. "$@"
}

# the inject tags are used to add Go struct tags for marshalling and unmarshalling custom field types e.g. MongoDB's BSON
# https://github.com/favadi/protoc-go-inject-tag
inject_tag() {
  for i in "$@"; do
    protoc-go-inject-tag -input="$i"
  done

}

# fields that are not needed are omitted for marshalling and umarshalling
#	XXX_NoUnkeyedLiteral struct{}          `json:"-" bson:"-"`
#	XXX_unrecognized     []byte            `json:"-" bson:"-"`
#	XXX_sizecache        int32             `json:"-" bson:"-"`
omit_bson_tag() {
  replace_lines "$1" 's/`json:"-"`/`json:"-" bson:"-"`/g'
}

omit_inject_tag() {
  replace_lines "$1" '/@inject_tag:/d'
  replace_lines "$1" '/inject tags/d'
  replace_lines "$1" '/protoc-go-inject-tag/d'
}

# replace lines using regex
# $1 the file(s)
# $2 the sed expression
replace_lines() {
  if [ "$(uname)" == "Darwin" ]; then
    sed -i '' -e "$2" "$1"/*.pb.go
  elif [ "$(expr substr "$(uname -s)" 1 5)" == "Linux" ]; then
    find "$1"/*.pb.go -type f -print0 | xargs -0 sed -i "$2"
  fi
}

# generate protoc for our service
echo "Generating internal proto..."
echo

## LIB PACKAGE
echo "Generating LIB PACKAGE"
make_pb_go ${LIB_ROOT}/authority.proto ${LIB_ROOT}/user.proto
omit_bson_tag ${LIB_ROOT}
omit_inject_tag ${LIB_ROOT}
echo "Done generating LIB PACKAGE"
echo "------------------------------------------------------------"
echo

## USER SERVICE
echo "Generating USER SERVICE"
make_pb_go ${USER_ROOT}/user.proto
inject_tag ${USER_ROOT}/user.pb.go
omit_bson_tag ${USER_ROOT}
omit_inject_tag ${USER_ROOT}
echo "Done generating USER SERVICE"
echo "------------------------------------------------------------"
echo

## DOCUMENT SERVICE
echo "Generating DOCUMENT SERVICE"
make_proto_set ${DOC_ROOT}/doc
make_pb_go ${DOC_ROOT}/doc.proto
inject_tag ${DOC_ROOT}/doc.pb.go
omit_bson_tag ${DOC_ROOT}
omit_inject_tag ${DOC_ROOT}
echo "Done generating DOCUMENT SERVICE"
echo "------------------------------------------------------------"
echo
