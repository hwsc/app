module gitlab.com/hwsc/app

require (
	github.com/Microsoft/go-winio v0.4.15-0.20190919025122-fc70bd9a86b5 // indirect
	github.com/Pallinder/go-randomdata v1.2.0
	github.com/cenkalti/backoff v2.1.1+incompatible // indirect
	github.com/containerd/containerd v1.3.0 // indirect
	github.com/containerd/continuity v0.0.0-20190426062206-aaeac12a7ffc // indirect
	github.com/docker/distribution v2.7.1+incompatible // indirect
	github.com/docker/docker v1.4.2-0.20191101170500-ac7306503d23 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang-migrate/migrate/v4 v4.2.4
	github.com/golang/protobuf v1.4.0
	github.com/gorilla/mux v1.7.3 // indirect
	github.com/gotestyourself/gotestyourself v2.2.0+incompatible // indirect
	github.com/hwsc-org/hwsc-api-blocks v0.0.0-20190811072423-e74c905d7c93
	github.com/hwsc-org/hwsc-lib v0.0.0-20200202175336-d3be23c7a93b
	github.com/klauspost/compress v1.10.4 // indirect
	github.com/lib/pq v1.3.0
	github.com/morikuni/aec v0.0.0-20170113033406-39771216ff4c // indirect
	github.com/oklog/ulid v1.3.1
	github.com/opencontainers/runc v0.1.1 // indirect
	github.com/ory/dockertest v3.3.4+incompatible
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.5.1
	go.mongodb.org/mongo-driver v1.3.2
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200420163511-1957bb5e6d1f // indirect
	golang.org/x/time v0.0.0-20190921001708-c4c64cad1fd0 // indirect
	google.golang.org/genproto v0.0.0-20200424135956-bca184e23272 // indirect
	google.golang.org/grpc v1.29.1
	google.golang.org/protobuf v1.21.0
)

go 1.14
